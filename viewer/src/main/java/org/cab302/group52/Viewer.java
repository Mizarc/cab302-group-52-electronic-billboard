package org.cab302.group52;

import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Viewer {
    static String xml = "";
    static Thread serverRunnable = new Thread(() -> {
        try {
            xml = CheckServer.checkServer();
            CheckServer.checkBillboard(xml);
        } catch (ClassNotFoundException | SQLException | IOException | InterruptedException e) {
            e.printStackTrace();
        }
    });

    public static void main(String[] args) throws SQLException, IOException, ClassNotFoundException {
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
        executor.scheduleAtFixedRate(serverRunnable, 0, 15, TimeUnit.SECONDS);
    }
}

