package org.cab302.group52;

import javax.swing.*;
import java.awt.event.*;

//function of class:
//Allows for the action of the escape key press or mouse clicked to exit program
public class Listener implements MouseListener {


    @Override
    public void mouseClicked(MouseEvent e) {
        System.exit(0);
    }

    //assigned action for key press
    static Action Closing = new AbstractAction() {
        public void actionPerformed(ActionEvent e) {
            System.exit(0);
        }
    };


    //unrelevant code beyond
    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

}
