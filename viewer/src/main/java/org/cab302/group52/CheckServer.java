package org.cab302.group52;

import org.cab302.group52.networkdata.schedule.*;
import org.w3c.dom.*;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.net.Socket;
import java.sql.SQLException;

class CheckServer {
    static String billboardColour = "", message = "", messageColour = "", picture = "", info = "", infoColour = "";
    static boolean exists = false;

    public static String checkServer() throws ClassNotFoundException, SQLException, IOException {
        Socket socket = new Socket("localhost", 3129);
        ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());

        ScheduleGetRequest scheduleRequest = new ScheduleGetRequest();
        oos.writeObject(scheduleRequest);
        oos.flush();

        ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
        Object o = ois.readObject();

        String xml = "";
        if (o instanceof ScheduleGetReply) {
            ScheduleGetReply scheduleReply = (ScheduleGetReply) o;
            xml = scheduleReply.getXML();
            oos.flush();
        }
        return xml;
    }

    public static void checkBillboard(String xml) throws InterruptedException {
        if (xml.equals("")) {
            SetupBillboard error = new SetupBillboard(false, "java.awt.Color[r=255,g=255,b=255]",
                    "No billboards scheduled!", "java.awt.Color[r=0,g=0,b=0]", "", "",
                    "");
            return;
        }
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db;
            db = dbf.newDocumentBuilder();
            Document doc = db.parse(new InputSource(new StringReader(xml)));
            doc.getDocumentElement().normalize();
            Element root = doc.getDocumentElement();

            billboardColour = root.getAttribute("background");
            message = root.getElementsByTagName("message").item(0).getTextContent();

            NamedNodeMap msgNM = root.getElementsByTagName("message").item(0).getAttributes();
            Node col = msgNM.getNamedItem("colour");
            messageColour = col.toString();

            NamedNodeMap picNM = root.getElementsByTagName("picture").item(0).getAttributes();
            Node urlNode = picNM.getNamedItem("url");
            if (urlNode == null) {
                Node dataNode = picNM.getNamedItem("data");
                picture = dataNode.toString();
                picture = SetupBillboard.findPictureData(picture);
            }
            else {
                picture = urlNode.toString();
                picture = SetupBillboard.findPictureData(picture);
            }

            info = root.getElementsByTagName("information").item(0).getTextContent();

            NamedNodeMap infoNM = root.getElementsByTagName("information").item(0).getAttributes();
            Node infoCol = infoNM.getNamedItem("colour");
            infoColour = infoCol.toString();

            SetupBillboard display = new SetupBillboard(false, billboardColour, message, messageColour, picture,
                    info, infoColour);

        } catch (Exception ignored) { }
    }
}
