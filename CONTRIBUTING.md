# Contributing to Group 52's Electronic Billboard

Hey lads, I'm establishing this guideline so that we're able to create a clean and consistent git repository. As each and every one of you has a personal preference as to how to name things and structure projects, we need to set that aside if we are to work together as a group. If you have any questions or want to modify the guidelines in some form or another, please contact me on Discord.

Signed, Kevin Rahardjo.


## How Do I Contribute?

### Code Contribution
For contributing directly to the code, please refer to the branch structure to ensure that your modifications are being sent to the correct area. Note that you cannot directly push to the master branch, master is strictly reserved for integrating all parts of the project together and working builds. If you believe that your build of the project is fit for integration testing, use the [merge request](https://gitlab.com/Mizarc/cab302-group-52-electronic-billboard/-/merge_requests) feature on the sidebar of the project page.

### Bug Reporting
If you've encountered an issue in a build of the program and are unable to fix it yourself, make use of the [issue page](https://gitlab.com/Mizarc/cab302-group-52-electronic-billboard/-/issues). Be sure to provide as much information as possible so as to reduce confusion and speed up the bug squashing process.


## How is the project structured?

### Branches
These are what each notable branch is used for.
- `master` holds the lastest development release. Ensure that it is able to be run at all costs. Do not commit to this directly.
- `restructuring` was used for moving project files around to suit the maven structure. Merged.
- `Control-Panel` held the control panel code pre restructuring. Merged.


## Style Guidelines

### Git Commits
- Keep your commits "atomic". If you can't describe the change in a short sentence, split the changes into multiple commits.
- Limit the subject line to 50 characters.
- Use the subject line to describe *what* change was made
- Utilise an imperative, present tense style (Say "Fix bug", not "Fixed bug" or "Fixing bug").
- Add a blank line between the subject line and the message body.
- Limit the message body to 72 characters per line.
- Use the message body to explain *why* and *how* that change was made.

### Java
This project loosely follows the [JavaRanch](https://coderanch.com/wiki/718799/Style) style guidelines as the official java guidelines haven't been updated since 1999 and contain many broken links. Unlike other guidelines such as the Google or Twitter style guides, this is closer to what the official guidelines provide. If you are using an IDE built for Java or use a Java extension in programs such as VS Code, you should be warned of any guidelines you are breaking. Some notable recommendations include:
- Braces are to be put on the end of the line, NOT on a newline