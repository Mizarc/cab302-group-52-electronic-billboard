# CAB302 Group 52 Electronic Billboard

The CAB302 major assignment for Group 52

## Contributers
- Andrew Yang = Andrew Yang (n9174745)
- Gabriel Nugent = Gabriel Nugent
- Mizarc = Kevin Rahardjo (n10489754)
- Sam Ireland = Samantha Ireland (n9994491)
- benjamin = Benjamin Giovannetti (n10632859)

## Requirements
- Java JDK 13+
- Maven
- Git

## Console Compile Instructions
1. Clone the master branch with `git clone https://gitlab.com/Mizarc/cab302-group-52-electronic-billboard.git`
2. Move into the cloned directory with `cd cab302-group52-electronic-billboard`
3. Package the project with `mvn -Dmaven.test.skip=true clean package`
4. Find the respective runnable jar files in <module>/target/ and run with `java -jar <name>.jar`