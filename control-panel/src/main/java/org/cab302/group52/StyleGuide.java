package org.cab302.group52;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * The purpose of this class is to group functions which can be reused by multiple classes, and to contain the
 * style settings to be used across the entire GUI.
 * */
public class StyleGuide {

    public static final String FONT = "Arial";
    private static final int[] FONT_SIZE = {15, 25, 30};
    private static final int[] TITLE_FONT_SIZE = {35, 45, 55};
    private static final Dimension[] BUTTON_SIZE = {new Dimension(130,50),
            new Dimension(200, 80), new Dimension(300, 80)};
    private static final Dimension FILLER_SIZE = new Dimension(100, 100);
    public static final Dimension PREVIEW_SIZE = new Dimension(600, 337);

    /**
     * Creates an int object based on the given values. Used for picking font sizes.
     * @param sizeArray The array to use containing different sizes in int form.
     * @param requestedSize The size to be implemented - can be Small, Medium, or Large.
     * @return An int with the requested size.
     */
    static private int sizePickerInt (int[] sizeArray, String requestedSize) {
        int size;
        if (requestedSize.equals("Small")) size = sizeArray[0];
        else if (requestedSize.equals("Large")) size = sizeArray[2];
        else size = sizeArray[1];
        return size;
    }

    /**
     * Creates a Dimension object based on the given values.
     * @param sizeArray The array to use containing different Dimension sizes.
     * @param requestedSize The size to be implemented - can be Small, Medium, or Large.
     * @return A Dimension with the requested size.
     */
    static private Dimension sizePickerDim (Dimension[] sizeArray, String requestedSize) {
        Dimension size;
        if (requestedSize.equals("Small")) size = sizeArray[0];
        else if (requestedSize.equals("Large")) size = sizeArray[2];
        else size = sizeArray[1];
        return size;
    }

    /**
     * Creates a button that conforms to the style guide's conventions.
     * @param text The text displayed on the button.
     * @param sizeOfButton The size of the button and its text - can be Small, Medium, or Large.
     * @return A button.
     */
    static public JButton setupBtn (String text, String sizeOfButton) {
        JButton button = new JButton(text);
        button.setPreferredSize(sizePickerDim(BUTTON_SIZE, sizeOfButton));
        button.setFont(new Font(FONT, Font.PLAIN, sizePickerInt(FONT_SIZE, sizeOfButton)));
        return button;
    }

    /**
     * Creates a main frame with specified width and height.
     * @param title The frame's title.
     * @param width  The frame's width.
     * @param height The frame's height.
     * @param resizable Whether or not the frame can be resized.
     * @param visible Whether or not the frame is visible upon initialisation - usually should be set to true, but
     *                there are a few instances where it needs to be made visible later.
     * @return A main frame with the given title, width, height, and variables.
     */
    static public JFrame setupMainFrame(String title, int width, int height, boolean resizable, boolean visible) {
        JFrame frame = new JFrame(title);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(width, height);
        frame.setLocationRelativeTo(null);
        frame.setResizable(resizable);
        frame.setVisible(visible);
        return frame;
    }

    /**
     * Creates a standard white panel.
     * @return A panel.
     */
    static public JPanel setupPanel() {
        JPanel panel = new JPanel();
        panel.setBackground(Color.white);
        return panel;
    }

    /**
     * Creates a panel with a colour other than white.
     * @param color The colour you want.
     * @return A panel with a custom colour.
     */
    static public JPanel setupPanel(Color color) {
        JPanel panel = new JPanel();
        panel.setBackground(color);
        return panel;
    }

    /**
     * Creates a panel with custom dimensions.
     * @param dimension The dimensions to be used.
     * @return A white panel with the given dimensions.
     */
    static public JPanel setupPanel(Dimension dimension) {
        JPanel panel = new JPanel();
        panel.setBackground(Color.white);
        panel.setPreferredSize(dimension);
        return panel;
    }

    /**
     * Creates generic filler panels to fill space in a GUI.
     * @param mainPnl The panel in need of filler.
     */
    static public void setupFillerPanels(JPanel mainPnl) {
        JPanel leftFill = setupPanel(FILLER_SIZE);
        mainPnl.add(leftFill, BorderLayout.WEST);
        JPanel rightFill = setupPanel(FILLER_SIZE);
        mainPnl.add(rightFill, BorderLayout.EAST);
    }

    /**
     * Creates labels for title panel.
     * @param text Text for label.
     * @param sizeOfFont The size of the text - can be Small, Medium, or Large.
     * @return A title label with the given text and font size.
     */
    static public JLabel setupTitle(String text, String sizeOfFont){
        JLabel title = new JLabel(text);
        title.setVerticalAlignment(JLabel.NORTH);
        title.setHorizontalAlignment(JLabel.CENTER);
        title.setFont(new Font(FONT, Font.BOLD, sizePickerInt(TITLE_FONT_SIZE, sizeOfFont)));
        title.setBorder(new EmptyBorder(30,10,10,10));
        return title;
    }

    /**
     * This method creates labels with no alignment.
     * @param text The text of the label.
     * @param sizeOfFont The size of the text - can be Small, Medium, or Large.
     * @return A label with the given text and font size.
     */
    static public JLabel setupLabel(String text, String sizeOfFont) {
        JLabel label = new JLabel(text);
        label.setFont(new Font(FONT, Font.PLAIN, sizePickerInt(FONT_SIZE, sizeOfFont)));
        return label;
    }

    /**
     * This method creates labels with alignment.
     * @param text The text of the label.
     * @param horAlign The desired horizontal alignment of the label (can be entered as a plain int, but best to use
     *                 JLabel.DIRECTION framework for compatibility).
     * @param verAlign The desired vertical alignment of the label (can be entered as a plain int, but best to use
     *                 JLabel.DIRECTION framework for compatibility).
     * @param sizeOfFont The size of the text - can be Small, Medium, or Large.
     * @return A label with the given text, alignment, and font size.
     */
    static public JLabel setupLabel(String text, int horAlign, int verAlign, String sizeOfFont) {
        JLabel label = new JLabel(text);
        label.setFont(new Font(FONT, Font.PLAIN, sizePickerInt(FONT_SIZE, sizeOfFont)));
        label.setHorizontalAlignment(horAlign);
        label.setVerticalAlignment(verAlign);
        return label;
    }

    /**
     * Creates a standard checkbox.
     * @param text The text of the checkbox.
     * @param sizeOfFont The size of the text - can be Small, Medium, or Large.
     * @return A checkbox with the given text and font size.
     */
    static public JCheckBox setupCheckbox(String text, String sizeOfFont) {
        JCheckBox checkbox = new JCheckBox(text);
        checkbox.setBackground(Color.white);
        checkbox.setFont(new Font(FONT, Font.BOLD, sizePickerInt(FONT_SIZE, sizeOfFont)));
        return checkbox;
    }

    /**
     * Creates a standard field for password entry.
     * @return A text field.
     */
    static public JTextField setupTField() {
        JTextField tField = new JTextField(20);
        tField.setBounds(160, 100, 120, 30);
        return tField;
    }

    /**
     * Creates a standard field for password entry.
     * @return A password field.
     */
    static public JPasswordField setupPField() {
        JPasswordField pField = new JPasswordField(20);
        pField.setBounds(160, 100, 120, 30);
        return pField;
    }

}