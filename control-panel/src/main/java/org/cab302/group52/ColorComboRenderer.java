package org.cab302.group52;

import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.MatteBorder;
import java.awt.*;

/**
 * Used for creating a combo box where a user can pick from a range of colours.
 */
public class ColorComboRenderer extends JPanel implements ListCellRenderer {
    //default colour
    static Color chosenColour = Color.black;

    /**
     * Initialises the renderer for the custom colour combo box.
     */
    public ColorComboRenderer() {
        super();
        setBorder(new CompoundBorder( new MatteBorder(2, 10, 2, 10, Color.white),
                new LineBorder(Color.black)));
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        if(value instanceof Color) chosenColour = (Color) value;
        return this;
    }

    /**
     * Sets the background colour of the given cell.
     * @param g The cell to be updated.
     */
    public void paint(Graphics g) {
        setBackground(chosenColour);
        super.paint(g);
    }

    /**
     * Converts a standard combo box into a colour combo box by iterating through most colours in the RGB spectrum.
     * @param cbColour The combo box to be converted.
     * @param type The type of combo box to be generated - either "font" or "background".
     * @return
     */
    static public JComboBox ComboBox(JComboBox cbColour, String type) {
        cbColour = new JComboBox();
        int[] values = new int[] {0, 129, 192, 255};
        int[] values2 = new int[] {255, 192, 129, 0};
        if(type.equals("font"))
            for (int r = 0; r < values.length; r++)
                for (int g = 0; g < values.length; g++)
                    for (int b = 0; b < values.length; b++) {
                        Color c = new Color(values[r], values[g], values[b]);
                        cbColour.addItem(c);
                    }
        else if(type.equals("background"))
            for (int r = 0; r < values2.length; r++)
                for (int g = 0; g < values2.length; g++)
                    for (int b = 0; b < values2.length; b++) {
                        Color c = new Color(values2[r], values2[g], values2[b]);
                        cbColour.addItem(c);
                    }
        cbColour.setRenderer(new ColorComboRenderer());
        return cbColour;
    }
}
