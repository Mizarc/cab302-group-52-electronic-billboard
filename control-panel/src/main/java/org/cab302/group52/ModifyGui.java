package org.cab302.group52;

import org.cab302.group52.networkdata.billboard.*;
import org.w3c.dom.*;
import org.xml.sax.InputSource;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.Socket;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Creates the billboard modification window, allowing the user to edit their own billboards.
 */
public class ModifyGui extends JFrame implements ActionListener {

    JButton modifyBtn = StyleGuide.setupBtn("Modify", "Medium"),
            backBtn = StyleGuide.setupBtn("Back", "Medium"),
            previewBtn = StyleGuide.setupBtn("Preview", "Medium"),
            importBtn = StyleGuide.setupBtn("Import", "Medium");
    JLabel  titleLabel = StyleGuide.setupLabel("Title: ", "Small"),
            messageLabel = StyleGuide.setupLabel("Message: ", "Small"),
            pictureLabel = StyleGuide.setupLabel("Picture: ", "Small"),
            infoLabel = StyleGuide.setupLabel("Information: ", "Small");
    JFrame modifyFrame = StyleGuide.setupMainFrame("Modify Billboard", 900, 900, false, true);
    JPanel mainPnl = StyleGuide.setupPanel(), btnPnl = StyleGuide.setupPanel(), optionPnl = StyleGuide.setupPanel(),
             billboardPnl = StyleGuide.setupPanel();
    JTextField titleText = StyleGuide.setupTField(), messageText = StyleGuide.setupTField(),
            pictureText = StyleGuide.setupTField(), infoText = StyleGuide.setupTField();
    JComboBox cbColour1, cbColour2, cbColour3;

    /**
     * Initialises the Modify button.
     */
    private void modifyBtn() {
        modifyBtn.addActionListener(this);
        btnPnl.add(modifyBtn);
    }

    /**
     * Initialises the Import button.
     */
    private void importBtn() {
        importBtn.addActionListener(this);
        btnPnl.add(importBtn);
    }

    /**
     * Adds text labels and fields to the GUI. These fields will auto-populate with existing data.
     */
    private void addLabels() {
        optionPnl.add(titleLabel);
        optionPnl.add(titleText);

        JLabel backgroundLabel = StyleGuide.setupLabel("Background Colour: ", "Small");
        optionPnl.add(backgroundLabel);
        String type = "background";
        cbColour1 = ColorComboRenderer.ComboBox(cbColour1, type);
        optionPnl.add(cbColour1);

        messageText = new JTextField(20);
        optionPnl.add(messageLabel);
        optionPnl.add(messageText);

        JLabel colourLabel = StyleGuide.setupLabel("Message Colour: ", "Small");
        optionPnl.add(colourLabel);
        String type2 = "font";
        cbColour2= ColorComboRenderer.ComboBox(cbColour2, type2);
        optionPnl.add(cbColour2);

        optionPnl.add(pictureLabel);
        optionPnl.add(pictureText);

        optionPnl.add(infoLabel);
        optionPnl.add(infoText);

        JLabel colourLabel2 = StyleGuide.setupLabel("Information Colour: ", "Small");
        optionPnl.add(colourLabel2);
        cbColour3 = ColorComboRenderer.ComboBox(cbColour3, type2);
        optionPnl.add(cbColour3);
    }

    /**
     * Sets up the window's main frame, panel, and title.
     */
    private void setupGui() {
        mainPnl.setLayout(new BoxLayout(mainPnl, BoxLayout.PAGE_AXIS));
        modifyFrame.add(mainPnl);

        JPanel titlePnl = StyleGuide.setupPanel();
        titlePnl.setPreferredSize(new Dimension(900, -550));
        titlePnl.setLayout(new FlowLayout(FlowLayout.CENTER, 10,0));
        mainPnl.add(titlePnl);

        JLabel title = StyleGuide.setupTitle("Modify Existing Billboard", "Large");
        titlePnl.add(title);
    }

    /**
     * Modifies the given billboard on the server.
     * @param billboardName The name of the billboard to modify.
     * @param message The new message for the billboard to display.
     * @param picture The new picture for the billboard to display.
     * @param info The new information for the billboard to display.
     * @param billboardColour The new colour for the billboard's background.
     * @param messageColour The new colour for the billboard's message text.
     * @param infoColour The new colour for the billboard's information text.
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws TransformerException
     */
    private void modifyBillboard(String id, String billboardName, String message, String picture, String info,
                                 String billboardColour, String messageColour, String infoColour)
                                throws IOException, ParserConfigurationException, TransformerException {
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.newDocument();

            Element rootElement = doc.createElement("billboard");
            doc.appendChild(rootElement);

            Attr billAttr = doc.createAttribute("background");
            billAttr.setValue(billboardColour);
            rootElement.setAttributeNode(billAttr);

            Element messageEle = doc.createElement("message");
            messageEle.appendChild(doc.createTextNode(message));
            rootElement.appendChild(messageEle);

            Attr msgAttr = doc.createAttribute("colour");
            msgAttr.setValue(messageColour);
            messageEle.setAttributeNode(msgAttr);

            Element pictureEle = doc.createElement("picture");
            rootElement.appendChild(pictureEle);

            if (picture.contains("http")) {
                Attr picAttr = doc.createAttribute("url");
                picAttr.setValue(picture);
                pictureEle.setAttributeNode(picAttr);
            }
            else {
                Attr picAttr = doc.createAttribute("data");
                picAttr.setValue(picture);
                pictureEle.setAttributeNode(picAttr);
            }

            Element infoEle = doc.createElement("information");
            infoEle.appendChild(doc.createTextNode(info));
            rootElement.appendChild(infoEle);

            Attr infoAttr = doc.createAttribute("colour");
            infoAttr.setValue(infoColour);
            infoEle.setAttributeNode(infoAttr);

            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");

            //Convert to XML to string
            StringWriter sw = new StringWriter();
            StreamResult result = new StreamResult(sw);
            DOMSource source = new DOMSource(doc);
            transformer.transform(source, result);
            String xmlString = sw.toString();

            Socket socket = new Socket("localhost", 3129);
            OutputStream outputStream = socket.getOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());

            BillboardModifyRequest modifyRequest = new BillboardModifyRequest(id, billboardName, xmlString);
            oos.writeObject(modifyRequest);
            oos.flush();

        } catch (ParserConfigurationException | DOMException | TransformerFactoryConfigurationError |
                IllegalArgumentException | TransformerException | IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Prefills a blank XML file.
     * @param id The ID of the XML file.
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public void prefillXML(String id) throws IOException, ClassNotFoundException {
        String billColor = "", message = "", msgColor = "", picture = "", info = "", infoColor = "";

        Socket socket = new Socket("localhost", 3129);
        OutputStream outputStream = socket.getOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());

        BillboardXMLRequest billboardXMLrequest = new BillboardXMLRequest(id);

        oos.writeObject(billboardXMLrequest);
        oos.flush();

        ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
        Object o = ois.readObject();

        if (o instanceof BillboardXMLReply){
            BillboardXMLReply billboardXMLReply = (BillboardXMLReply) o;

            String xml = billboardXMLReply.getXML();

            try {
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db;
                db = dbf.newDocumentBuilder();
                Document doc = db.parse(new InputSource(new StringReader(xml)));
                doc.getDocumentElement().normalize();
                Element root = doc.getDocumentElement();

                message = root.getElementsByTagName("message").item(0).getTextContent();
                messageText.setText(message);

                NamedNodeMap picNM = root.getElementsByTagName("picture").item(0).getAttributes();
                Node urlNode = picNM.getNamedItem("url");
                if(urlNode == null){
                    Node dataNode = picNM.getNamedItem("data");
                    picture = dataNode.toString();
                    picture = SetupBillboard.findPictureData(picture);
                } else {
                    picture = urlNode.toString();
                    picture = SetupBillboard.findPictureData(picture);
                }

                pictureText.setText(picture);

                info = root.getElementsByTagName("information").item(0).getTextContent();
                infoText.setText(info);
            } catch (Exception e) { }
            oos.flush();
        }
    }

    /**
     * Sets up the window's GUI. Adds ActionListeners to the buttons. Will also determine where the admin user came from
     * to see if they can pick a billboard to edit, or can only edit the one they chose from the list of billboards.
     */
    public ModifyGui() throws IOException, ClassNotFoundException {
        super("Modify GUI");
        setupGui();

        billboardPnl.setLayout(new FlowLayout(FlowLayout.CENTER, 35,5));
        billboardPnl.setPreferredSize(new Dimension(300,-50));
        mainPnl.add(billboardPnl);

        GridLayout optionGrid = new GridLayout(22,0);
        optionGrid.setVgap(15);
        optionPnl.setLayout(optionGrid);
        optionPnl.setPreferredSize(new Dimension(300,900));
        billboardPnl.add(optionPnl);

        addLabels();

        btnPnl.setPreferredSize(new Dimension(50, -550));
        btnPnl.setLayout(new FlowLayout(FlowLayout.CENTER, 10,15));
        mainPnl.add(btnPnl);

        backBtn.addActionListener(this);
        btnPnl.add(backBtn);

        if(MainGui.permissions[1].equals("Modify") && !ListBillboard.fromList){ importBtn(); }
        else {
            String id = ListBillboard.data[0];
            titleText.setText(ListBillboard.data[1]);
            prefillXML(id);
            ListBillboard.fromList = false; // reset variable
        }

        previewBtn.addActionListener(this);
        btnPnl.add(previewBtn);
        modifyBtn();
    }

    /**
     * Checks to see if a given title exists in the server.
     * @param title The title to be located.
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws IOException
     * @return True if found, false if not.
     */
    private boolean checkTitle(String id, String title) throws ClassNotFoundException, SQLException, IOException{
        boolean isThere = false;
        Socket socket = new Socket("localhost", 3129);
        ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());

        BillboardListRequest billboardListRequest = new BillboardListRequest();
        oos.writeObject(billboardListRequest);
        oos.flush();

        ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
        Object o = ois.readObject();

        if (o instanceof BillboardListReply) {
            BillboardListReply billboardListReply = (BillboardListReply) o;
            ArrayList<String> billboardName = billboardListReply.getBillboardName();
            ArrayList<String> billboardId = billboardListReply.getBillboardId();

            if (id.equals("")) for (String s : billboardName) if (title.equals(s)) {
                isThere = true;
                break;
            }
            else for (int i = 0; i < billboardName.size(); i++) {
                    if (title.equals(billboardName.get(i)) && id.equals(billboardId.get(i))) isThere = false;
                    else isThere = title.equals(billboardName.get(i)) && !id.equals(billboardId.get(i));
                }
            oos.flush();
        }
        return isThere;
    }

    boolean isSuccessful = false;
    boolean isThere = false;

    @Override
    public void actionPerformed(ActionEvent e) {
        Object src = e.getSource();

        if (src == backBtn) {
            MainGui main = new MainGui();
            modifyFrame.setVisible(false);
        }
        else if (src == importBtn) {
            try {
                String billboardName = titleText.getText();
                String id = "";
                isSuccessful = checkTitle(id, billboardName);
            } catch (ClassNotFoundException | SQLException | IOException classNotFoundException) {
                classNotFoundException.printStackTrace();
            }
            if (isSuccessful) JOptionPane.showMessageDialog(modifyFrame,
                    "Billboard retrieved successfully from database.");
            else JOptionPane.showMessageDialog(modifyFrame, "Billboard does not exist in database",
                    "Billboard Request Error", JOptionPane.ERROR_MESSAGE);
        }
        else if (src == modifyBtn) {
            String id = (ListBillboard.data[0]);
            String billboardName = titleText.getText();
            String message = messageText.getText();
            String picture = pictureText.getText();
            String info = infoText.getText();
            Object colourBillboard = cbColour1.getItemAt(cbColour1.getSelectedIndex());
            String colourBillStr = colourBillboard.toString();
            Object colourMsg = cbColour2.getItemAt(cbColour2.getSelectedIndex());
            String colourMsgStr = colourMsg.toString();
            Object colourInfo = cbColour3.getItemAt(cbColour3.getSelectedIndex());
            String colourInfoStr = colourInfo.toString();
            try {
                isThere = checkTitle(id, billboardName);
                if (!isThere) {
                    modifyBillboard(id,billboardName, message, picture,info, colourBillStr, colourMsgStr, colourInfoStr);
                    JOptionPane.showMessageDialog(modifyFrame, "Billboard updated successfully in database.");
                    isThere = false;
                }
                else JOptionPane.showMessageDialog(modifyFrame, "Billboard title already exists in database, " +
                        "please change to a unique title", "Unique Title Error",JOptionPane.ERROR_MESSAGE);
            } catch (IOException | ParserConfigurationException | TransformerException | SQLException |
                    ClassNotFoundException ioException) {
                ioException.printStackTrace();
            }
        }
        else if (src == previewBtn) {
            String message = messageText.getText();
            String picture = pictureText.getText();
            String info = infoText.getText();
            Object colourBillboard = cbColour1.getItemAt(cbColour1.getSelectedIndex());
            String colourBillStr = colourBillboard.toString();
            Object colourMsg = cbColour2.getItemAt(cbColour2.getSelectedIndex());
            String colourMsgStr = colourMsg.toString();
            Object colourInfo = cbColour3.getItemAt(cbColour3.getSelectedIndex());
            String colourInfoStr = colourInfo.toString();

            try {
                SetupBillboard previewWindow = new SetupBillboard(true, colourBillStr, message, colourMsgStr,
                        picture, info, colourInfoStr);
            } catch (InterruptedException interruptedException) {
                interruptedException.printStackTrace();
            }
        }
    }
}
