package org.cab302.group52;

import org.cab302.group52.networkdata.user.*;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.security.MessageDigest;
import java.util.ArrayList;

/**
 * Creates the edit users window, for editing the different user accounts on the server.
 */
public class UsersGui extends JFrame implements ActionListener {
    
    JFrame scheduleFrame = StyleGuide.setupMainFrame("Edit Users", 1100, 900, false, true),
            createFrame = StyleGuide.setupMainFrame("Create New User", 700, 700, false, false);
    JPanel mainPnl = StyleGuide.setupPanel(), btnPnl = StyleGuide.setupPanel(), listPnl = StyleGuide.setupPanel(),
            centerPnl = StyleGuide.setupPanel(), botPnl = StyleGuide.setupPanel();
    JButton backBtn = StyleGuide.setupBtn("Back", "Medium"),
            createBtn = StyleGuide.setupBtn("Create", "Medium"),
            newUserBtn = StyleGuide.setupBtn("Create New User", "Large"),
            updateBtn = StyleGuide.setupBtn("Update User", "Large"),
            deleteBtn = StyleGuide.setupBtn("Delete User", "Large");
    JTable table;
    JLabel usernameLabel = StyleGuide.setupLabel("Username: ", "Small"),
            passwordLabel = StyleGuide.setupLabel("Password: ", "Small"),
            permissionsLabel = StyleGuide.setupLabel("Permissions: ", "Small");
    JTextField usernameText = StyleGuide.setupTField();
    JTextField passwordText = StyleGuide.setupPField();
    JCheckBox p1 = StyleGuide.setupCheckbox("Create Billboards", "Small"),
            p2 = StyleGuide.setupCheckbox("Edit All Billboards", "Small"),
            p3 = StyleGuide.setupCheckbox("Schedule Billboards", "Small"),
            p4 = StyleGuide.setupCheckbox("Edit Users", "Small");

    static String[] data = null;

    /**
     * Initialises the Back button.
     */
    private void backBtn() {
        backBtn.addActionListener(this);
        btnPnl.add(backBtn);
    }

    /**
     * Initialises the Create button.
     */
    private void createBtn() {
        createBtn.addActionListener(this);
        btnPnl.add(createBtn);
    }

    /**
     * Initialises the Create New User button.
     */
    private void newUserBtn() {
        newUserBtn.addActionListener(this);
        botPnl.add(newUserBtn);
    }

    /**
     * Initialises the Delete User button.
     */
    private void deleteBtn() {
        deleteBtn.addActionListener(this);
        btnPnl.add(deleteBtn);
    }

    /**
     * Initialises the Update User button.
     */
    private void updateBtn(){
        updateBtn.addActionListener(this);
        btnPnl.add(updateBtn);
    }

    /**
     * Creates a user on the server with the given details.
     * @param username The username for the new user.
     * @param hashedPassword The password for the new user (in hashed form).
     * @param createBillboard Permission setting for whether they can or can't create billboards.
     * @param editBillboard Permission setting for whether they can or can't edit billboards.
     * @param scheduleBillboard Permission setting for whether they can or can't schedule billboards.
     * @param editUser Permission setting for whether they can or can't edit users.
     * @throws IOException
     */
    private void CreateUser(String username, String hashedPassword, boolean createBillboard, boolean editBillboard,
                            boolean scheduleBillboard, boolean editUser ) throws IOException {
        Socket socket = new Socket("localhost", 3129);
        ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
        UserAddRequest createUser = new UserAddRequest(username,hashedPassword,createBillboard,editBillboard,scheduleBillboard,editUser);
        oos.writeObject(createUser);
        oos.flush();
        oos.close();
        socket.close();
    }

    /**
     * Sets up the window's main frame, panel, and title.
     */
    private void setupGui() {
        mainPnl.setLayout(new BoxLayout(mainPnl, BoxLayout.PAGE_AXIS));
        scheduleFrame.add(mainPnl);

        JPanel titlePnl = StyleGuide.setupPanel();
        titlePnl.setPreferredSize(new Dimension(900, -500));
        titlePnl.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 0));
        mainPnl.add(titlePnl);

        JLabel title = StyleGuide.setupTitle("Edit User Database", "Large");
        titlePnl.add(title);
    }

    /**
     * Converts hashes to strings. Passwords are stored as hashes, but must be converted to strings before they can be
     * changed.
     * @param hash The hash to convert.
     * @return A hash as a string.
     */
    private static String byteToString(byte[] hash) {
        StringBuffer sb = new StringBuffer();
        for(byte b : hash) sb.append(String.format("%02x",b & 0xFF));
        return sb.toString();
    }

    /**
     * Creates all of the necessary labels for the new user portion of the GUI.
     */
    private void newUserLabels() {
        centerPnl.add(usernameLabel);
        centerPnl.add(usernameText);

        centerPnl.add(passwordLabel);
        centerPnl.add(passwordText);
        enableInputMethods(true);

        centerPnl.add(permissionsLabel);
        centerPnl.add(p1);
        centerPnl.add(p2);
        centerPnl.add(p3);
        centerPnl.add(p4);
    }

    /**
     * The GUI to create new users. Changes will only be saved if the newUserBtn is pressed.
     */
    private void createGui() {
        createFrame.setVisible(true);

        JPanel mainPnl = StyleGuide.setupPanel();
        BorderLayout layout = new BorderLayout();
        layout.setVgap(20);
        mainPnl.setLayout(layout);
        createFrame.add(mainPnl);

        JPanel titlePnl = StyleGuide.setupPanel();
        mainPnl.add(titlePnl, BorderLayout.NORTH);
        JLabel title = StyleGuide.setupTitle("Add New User's Details", "Medium");
        titlePnl.add(title);

        GridLayout textGrid = new GridLayout(9, 0);
        textGrid.setHgap(35);
        centerPnl.setLayout(textGrid);
        mainPnl.add(centerPnl, BorderLayout.CENTER);
        newUserLabels();

        StyleGuide.setupFillerPanels(mainPnl);
        mainPnl.add(botPnl, BorderLayout.SOUTH);
        newUserBtn();
    }

    /**
     * Sets up the window's GUI. Adds ActionListeners to the buttons.
     */
    public UsersGui() throws IOException, ClassNotFoundException {
        setupGui();

        listPnl.setPreferredSize(new Dimension(1000, 50));
        mainPnl.add(listPnl);
        loadUsers();

        btnPnl.setPreferredSize(new Dimension(50, -500));
        btnPnl.setLayout(new FlowLayout(FlowLayout.LEFT, 10,15));
        mainPnl.add(btnPnl);

        backBtn();
        createBtn();
        updateBtn();
        deleteBtn();
    }

    /**
     * Loads all of the users from the server as a table. The cells of the table cannot be edited, but data is
     * extracted from them when they are selected. The table can be scrolled through if it is too long.
     * @throws IOException
     * @throws ClassNotFoundException
     */
    void loadUsers() throws IOException, ClassNotFoundException {

        boolean loadUsers = true;
        Socket socket = new Socket("localhost", 3129);
        ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());

        UserListRequest userListRequest = new UserListRequest(loadUsers);
        oos.writeObject(userListRequest);
        oos.flush();

        ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
        Object o = ois.readObject();

        if (o instanceof UserListReply){
            UserListReply userListReply = (UserListReply) o;
            ArrayList<String> userId = userListReply.getUserId();
            ArrayList<String> usernames = userListReply.getUsername();
            ArrayList<String> passwords = userListReply.getPasswords();
            ArrayList<String> createb = userListReply.getCreateBillboad();
            ArrayList<String> editb = userListReply.getEditBillboard();
            ArrayList<String> scheduleb = userListReply.getScheduleBillboard();
            ArrayList<String> editu = userListReply.getEditUsers();

            String[] columnNames = {"User Id", "Username","Password","Create","Modify","Schedule","Edit users"};

            String[][] finalData = new String[usernames.size()][7];

            for (int i = 0; i < usernames.size(); i++) {
                finalData[i][0] = userId.get(i);
                finalData[i][1] = usernames.get(i);
                finalData[i][2] = passwords.get(i);
                finalData[i][3] = createb.get(i);
                finalData[i][4] = editb.get(i);
                finalData[i][5] = scheduleb.get(i);
                finalData[i][6] = editu.get(i);
            }

            DefaultTableModel tableModel = new DefaultTableModel(finalData, columnNames) {
                @Override
                public boolean isCellEditable(int row, int column) {
                    return true;
                }
            };

            table = new JTable(finalData, columnNames);
            table.setModel(tableModel);
            table.setCellSelectionEnabled(true);
            JScrollPane scrollP = new JScrollPane(table);
            listPnl.add(scrollP);

            table.addMouseListener(new MouseAdapter() {
                public void mouseClicked(MouseEvent e) {

                    data = new String[7];
                    int selectedRow = table.getSelectedRow();
                    data[0] = table.getValueAt(selectedRow, 0).toString();
                    data[1] = table.getModel().getValueAt(selectedRow, 1).toString();
                    data[2] = table.getModel().getValueAt(selectedRow, 2).toString();
                    data[3] = table.getModel().getValueAt(selectedRow, 3).toString();
                    data[4] = table.getModel().getValueAt(selectedRow, 4).toString();
                    data[5] = table.getModel().getValueAt(selectedRow, 5).toString();
                    data[6] = table.getModel().getValueAt(selectedRow,6).toString();
                    System.out.println(data[0]);
                }
            });
            oos.flush();
        }
    }
    public void UpdateSelected(String id, String create, String modify, String schedule, String edit ) throws IOException {

        Socket socket = new Socket("localhost", 3129);
        ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());

        UserModifyRequest updateUserRequest = new UserModifyRequest(id,create,modify,schedule,edit);
        oos.writeObject(updateUserRequest);
        oos.flush();

    }

    public void DeleteSelected(String userId) throws IOException, ClassNotFoundException {

        Socket socket = new Socket("localhost", 3129);
        OutputStream outputStream = socket.getOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());


        UserRemoveRequest deleteUserRequest = new UserRemoveRequest(userId);
        oos.writeObject(deleteUserRequest);
        System.out.println(deleteUserRequest);
        oos.flush();

        ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
        Object o = ois.readObject();
        if(o instanceof UserRemoveReply){
            UserRemoveReply deleteUserReply = (UserRemoveReply) o;
            //boolean deleteduser = deleteUserReply.isDeleteUserSuccessful();
            if(deleteUserReply.isSuccessful()) {
                JOptionPane.showMessageDialog(this, "User :" + userId + " deleted",
                        "Deleted", JOptionPane.PLAIN_MESSAGE);

            }


            new UsersGui();
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object src = e.getSource();

        if (src == backBtn) {
            MainGui main = new MainGui();
            scheduleFrame.setVisible(false);
        }
        else if (src == createBtn) createGui();
        else if (src == newUserBtn) {
            createFrame.dispose();
            try {
                //Hash passwordfield
                MessageDigest md = MessageDigest.getInstance("SHA-256");
                String password1 = passwordText.getText();

                System.out.println(password1);
                byte[] hashedPassword = md.digest(password1.getBytes());
                // Send parameters to createuser function
                String hashedpassword = byteToString(hashedPassword);
                System.out.println("From Create User   " + hashedpassword);
                String username = usernameText.getText();
                boolean createBillboards = p1.isSelected();
                boolean editBillboards = p2.isSelected();
                boolean scheduleBillboards = p3.isSelected();
                boolean editUsers = p4.isSelected();
                CreateUser(username,hashedpassword,createBillboards,editBillboards,scheduleBillboards,editUsers);
                JOptionPane.showMessageDialog(this, "User :" + username + " Created",
                        "Created", JOptionPane.PLAIN_MESSAGE);
                scheduleFrame.setVisible(false);
                UsersGui main = new UsersGui();
            } catch (Exception E) { System.out.println("User not created"); }
        }
        else if (src == updateBtn) {
            try {
                int selectedRow = table.getSelectedRow();
                String id = table.getValueAt(selectedRow,0).toString();
                String create = table.getValueAt(selectedRow, 3).toString();
                String modify = table.getValueAt(selectedRow, 4).toString();
                String schedule = table.getValueAt(selectedRow, 5).toString();
                String edit = table.getValueAt(selectedRow, 6).toString();

                UpdateSelected(id,create,modify,schedule,edit);

                JOptionPane.showMessageDialog(this, "New permissions updated",
                        "Permissions", JOptionPane.PLAIN_MESSAGE);
                scheduleFrame.setVisible(false);
                UsersGui main = new UsersGui();
            } catch(Exception ex) { }
        }
        else if (src == deleteBtn) {
            try {
                int selectedRow = table.getSelectedRow();
                String userId =  data[0] = table.getValueAt(selectedRow, 0).toString();
                System.out.println(userId);
                DeleteSelected(userId);

                scheduleFrame.setVisible(false);
                UsersGui main = new UsersGui();
            } catch (IOException | ClassNotFoundException ioException) { ioException.printStackTrace(); }
        }
    }
}
