package org.cab302.group52;

import org.cab302.group52.networkdata.billboard.*;
import org.cab302.group52.networkdata.schedule.*;

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.*;

/**
 * Creates the billboard scheduling window, allowing the user to choose when to display billboards.
 */
public class ScheduleGui extends JFrame implements ActionListener{
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    JFrame scheduleFrame = StyleGuide.setupMainFrame("Billboard Schedule", screenSize.width-50, screenSize.height-75, false, true);
    JPanel mainPnl = StyleGuide.setupPanel(), btnPnl = StyleGuide.setupPanel(), calendarPnl, weekPnl = StyleGuide.setupPanel();
    JButton backBtn = StyleGuide.setupBtn("Back", "Medium"), scheduleBtn = StyleGuide.setupBtn("Schedule Billboard", "Large");
    JPanel monPnl, tuePnl, wedPnl, thurPnl, friPnl, satPnl, sunPnl, optionPnl;
    JTextField duration;
    JComboBox recurList;
    JList billList, dayList, hourList, minList;
    JLayeredPane centerPane;
    String[] recurStr = {"No", "Every Day", "Every Hour", "Every _ Minutes"};

    Date today = new Date();
    String date;
    String[] days =  new String[7];
    String[] hours = new String[25];
    String[] minutes = new String[60];

    /**
     * Sets up the arrays that contain hours and minutes.
     */
    private void setupStrings() {
        for (int i = 0; i < 25; i++) hours[i] = Integer.toString(i);
        for (int i = 0; i < 60; i++) minutes[i] = Integer.toString(i);
    }

    /**
     * Sets up the window's main frame, panel, and title.
     */
    private void setupGui() {
        mainPnl.setLayout(new BoxLayout(mainPnl, BoxLayout.PAGE_AXIS));
        scheduleFrame.add(mainPnl);

        JPanel titlePnl = StyleGuide.setupPanel();
        titlePnl.setPreferredSize(new Dimension(900, -500));
        titlePnl.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 0));
        mainPnl.add(titlePnl);
        JLabel title = StyleGuide.setupTitle("Schedule Showing of Billboards", "Large");
        titlePnl.add(title);

        weekPnl.setPreferredSize(new Dimension(900, -620));
        weekPnl.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 0));
        weekPnl.setBackground(Color.white);
        mainPnl.add(weekPnl);

        setupStrings();
    }

    /**
     * Sets up the panel to display the week.
     * @param date The date to be displayed.
     */
    private void weekSet(String date){
        setupWeekPnl(date);
        addPanels();
    }

    private void grabVar() throws ClassNotFoundException, SQLException, IOException{
        Socket socket = new Socket("localhost", 3129);
        ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());

        ScheduleListRequest scheduleListRequest = new ScheduleListRequest();
        oos.writeObject(scheduleListRequest);
        oos.flush();

        ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
        Object o = ois.readObject();

        if (o instanceof ScheduleListReply) {
            ScheduleListReply scheduleListReply = (ScheduleListReply) o;
            ArrayList<Integer> id = scheduleListReply.getId();
            ArrayList<Integer> billboardId = scheduleListReply.getBillboardId();
            ArrayList<java.sql.Date> billboardDate = scheduleListReply.getDate();
            ArrayList<Integer> billboardTime = scheduleListReply.getTime();
            ArrayList<Integer> billboardDuration = scheduleListReply.getDuration();
            ArrayList<Integer> billboardRecur = scheduleListReply.getRecurType();
            ArrayList<Integer> billboardRecurMin = scheduleListReply.getRecurInterval();


            for (int i = 0; i < billboardId.size(); i++) {
                String ID = String.valueOf(id.get(i));
                String billId = String.valueOf(billboardId.get(i));

                java.sql.Date dateCurrent = billboardDate.get(i);
                DateFormat sdf = new SimpleDateFormat("E, d MMM");
                String day = sdf.format(dateCurrent);


                //finalSchedule[i][2] = day;
                String startTime = String.valueOf(billboardTime.get(i));
                String duration = String.valueOf(billboardDuration.get(i));

                int recurI = billboardRecur.get(i);
                String recurS = "";
                if (recurI == 1){
                    recurS = recurStr[0];
                }
                else if (recurI == 2){ // Every day
                    recurS = recurStr[1];
                }
                else if (recurI == 3){ // Every hour
                    recurS = recurStr[2];
                }
                else if (recurI == 4){// Every x minutes
                    recurS = recurStr[3];
                }


                String recurMinute = String.valueOf(billboardRecurMin.get(i));

                int hour = Integer.parseInt(startTime)/ 60;
                int minute = Integer.parseInt(startTime) % 60;


                if (recurS.equals(recurStr[0])) {
                    Random rd = new Random();
                    int r = rd.nextInt(((230 - 1) + 1) + 1);
                    int g = rd.nextInt(((230 - 1) + 1) + 1);
                    int b = rd.nextInt(((230 - 1) + 1) + 1);

                    addPnl(String.valueOf(billboardId.get(i)), day, hour, minute, String.valueOf(billboardDuration.get(i)), r, g, b);

                }
                else{
                    recurPnl(String.valueOf(billboardId.get(i)), day, hour, minute, String.valueOf(billboardDuration.get(i)), recurS, String.valueOf(billboardRecurMin.get(i)));
                }

            }

            oos.flush();
        }

    }

    static int singleHeight = 24;
    static int pnlWidth = 226;
    
    private void addPnl(String title, String day, int hour, int minute, String time, Integer r, Integer g, Integer b) {
        int hourCount = 0;
        int timeInt = Integer.parseInt(time);
        double minutes, minuteCount;
        if (Integer.parseInt(time) >= 60){
            int times = timeInt/60;
            hourCount = times;
            minutes = timeInt - 60.0*times;
            minuteCount = (minutes/60.0)*24.0;

        }
        else minuteCount = (timeInt/60.0)*24.0;
        double heightTime = (hourCount*24.0) + minuteCount;

        int startHour;
        startHour = (hour*singleHeight);

        double startMinute;
        startMinute = (minute/60.0)*24.0;

        double startTime;
        startTime = startHour + startMinute  + 27.0;

        int startWidth = 0;
        int startGap = 5;
        int betweenGap = 6;

        SimpleDateFormat sdf = new SimpleDateFormat("E, d MMM");
        Calendar c2 = Calendar.getInstance();
        String current = sdf.format(c2.getTime());

        c2.add(Calendar.DAY_OF_MONTH, 1);
        String day2 = sdf.format(c2.getTime());

        c2.add(Calendar.DAY_OF_MONTH, 1);
        String day3 = sdf.format(c2.getTime());

        c2.add(Calendar.DAY_OF_MONTH, 1);
        String day4 = sdf.format(c2.getTime());

        c2.add(Calendar.DAY_OF_MONTH, 1);
        String day5 = sdf.format(c2.getTime());

        c2.add(Calendar.DAY_OF_MONTH, 1);
        String day6 = sdf.format(c2.getTime());

        c2.add(Calendar.DAY_OF_MONTH, 1);
        String day7 = sdf.format(c2.getTime());

        //assign start width based on day
        if (day.equals(current)) startWidth = startGap;
        else if (day.equals(day2)) startWidth = startGap + betweenGap + pnlWidth;
        else if (day.equals(day3)) startWidth = startGap + betweenGap*2 + pnlWidth*2;
        else if (day.equals(day4)) startWidth = startGap + betweenGap*3 + pnlWidth*3;
        else if (day.equals(day5)) startWidth = startGap + betweenGap*4 + pnlWidth*4;
        else if (day.equals(day6)) startWidth = startGap + betweenGap*5 + pnlWidth*5;
        else if (day.equals(day7)) startWidth = startGap + betweenGap*6 + pnlWidth*6;

        JPanel pnl = new JPanel();
        pnl.setBounds(startWidth, (int)startTime, pnlWidth, (int)heightTime);
        pnl.setBackground(new Color(r, g, b, 200));
        pnl.setBorder(BorderFactory.createLineBorder(Color.black));

        JLabel name = new JLabel(title);
        name.setHorizontalAlignment(SwingConstants.CENTER);
        pnl.add(name);

        centerPane.add(pnl, JLayeredPane.POPUP_LAYER);
    }

    private void recurPnl(String title, String day, int hour, int minute, String time, String recur, String recurMin){
        SimpleDateFormat sdf = new SimpleDateFormat("E, d MMM");
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_MONTH, 7);
        String weekEnd = sdf.format(c.getTime());

        String current;
        Calendar c2 = Calendar.getInstance(); //needs to match day
        current = sdf.format(c2.getTime());
        while (!current.equals(day)) {
            c2.add(Calendar.DAY_OF_MONTH, 1);
            current = sdf.format(c2.getTime());
        }

        Random rd = new Random();
        int r = rd.nextInt(((230 - 1) + 1) + 1);
        int g = rd.nextInt(((230 - 1) + 1) + 1);
        int b = rd.nextInt(((230 - 1) + 1) + 1);

        if (recur.equals(recurStr[1]))
            while (!day.equals(weekEnd)) {
                addPnl(title, day, hour, minute, time, r, g, b);
                c2.add(Calendar.DAY_OF_MONTH, 1);
                day = sdf.format(c2.getTime());
            }
        else if (recur.equals(recurStr[2])) for(int i = hour; i < 25; i++) addPnl(title, day, i, minute, time, r, g, b);
        else if (recur.equals(recurStr[3])) {
            int recurInt = Integer.parseInt(recurMin);
            int minInt;
            int timeInt = Integer.parseInt(time);
            int minuteTime;

            if (recurInt >= 60) {

                while (hour <= 24) {
                    addPnl(title, day, hour, minute, time, r, g, b);

                    int totalTime = timeInt + recurInt;
                    int hourTime = (totalTime/60) + hour;
                    int minTime = (totalTime-60) + minute;
                    if ((minTime) < 60) {
                        hour = hourTime;
                        minute = minTime;
                    }
                    else {
                        hour = ((minTime) / 60) + hourTime;
                        minute = (minTime-((minTime) / 60) * 60);
                    }
                }
            }
            else {
                int total = 25 * (60 / recurInt);
                int overall = total - (hour * (60 / recurInt));
                for (int i = 0; i < overall; i++) {
                    addPnl(title, day, hour, minute, time, r, g, b);
                    minInt = minute;
                    recurInt = Integer.parseInt(recurMin);

                    int hourTime = (minInt + recurInt)/60;
                    if ((minInt + recurInt) < 60) minuteTime = (minInt + recurInt);
                    else minuteTime = (minInt + recurInt) - 60;

                    hour = hourTime + hour;
                    minute = minuteTime;
                }
            }
        }
    }

    private void setupWeekPnl(String date) {
        JLabel dateLbl = new JLabel(date);
        dateLbl.setFont(new Font(StyleGuide.FONT, Font.BOLD, 20));
        weekPnl.add(dateLbl);
    }

    private JPanel setupDayPanel(Color colour) {
        JPanel pnl = new JPanel();
        pnl.setLayout(new GridLayout(26,0, -1, -1));
        pnl.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
        pnl.setBackground(colour);
        return pnl;
    }

    private void setupDayLabel(JPanel jPanel, String text) {
        JLabel label;
        JLabel lab = new JLabel(text);
        lab.setHorizontalAlignment(JLabel.CENTER);
        jPanel.add(lab);
        for (int i = 0; i<(25); i++) {
            if (i == 0 || i == 6 || i == 12 || i == 18 || i == 24) label = new JLabel(Integer.toString(i));
            else label = new JLabel("");
            label.setBorder(BorderFactory.createLineBorder(Color.black));
            jPanel.add(label);
        }
    }

    private void dayLabels(Calendar c, int num, JPanel pnl, JPanel main) {
        SimpleDateFormat sdf = new SimpleDateFormat("E, d MMM");
        c.add(Calendar.DAY_OF_MONTH, 1);
        String newDate = sdf.format(c.getTime());

        days[num] = newDate;

        pnl = setupDayPanel(Color.white);
        main.add(pnl);
        setupDayLabel(pnl, newDate);
    }

    private void addPanels() {
        SimpleDateFormat sdf = new SimpleDateFormat("E, d MMM");
        Calendar c = Calendar.getInstance();
        String now = sdf.format(c.getTime());

        days[0] = now;

        monPnl = setupDayPanel(Color.white);
        calendarPnl.add(monPnl);
        setupDayLabel(monPnl,now);

        dayLabels(c, 1, tuePnl, calendarPnl);
        dayLabels(c, 2, wedPnl, calendarPnl);
        dayLabels(c, 3, thurPnl, calendarPnl);
        dayLabels(c, 4, friPnl, calendarPnl);
        dayLabels(c, 5, satPnl, calendarPnl);
        dayLabels(c, 6, sunPnl, calendarPnl);
    }

    private void returnTitles() throws ClassNotFoundException, SQLException, IOException {
        Socket socket = new Socket("localhost", 3129);
        ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());

        BillboardListRequest billboardListRequest = new BillboardListRequest();
        oos.writeObject(billboardListRequest);
        oos.flush();

        ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
        Object o = ois.readObject();

        if (o instanceof BillboardListReply) {
            BillboardListReply billboardListReply = (BillboardListReply) o;
            ArrayList<String> billboardName = billboardListReply.getBillboardName();

            String[] strArray = new String[billboardName.size()];
            for (int i = 0; i < billboardName.size(); i++) strArray[i] = billboardName.get(i);
            billList = new JList(strArray);
            oos.flush();
        }
        billList.setSelectedIndex(0);
        optionPnl.add(new JScrollPane(billList));
    }

    /**
     * Sets up the part of the Schedule GUI that contains options for the user to fill.
     * @throws SQLException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void setupOptions() throws SQLException, IOException, ClassNotFoundException {

        optionPnl = new JPanel();
        optionPnl.setLayout(new GridLayout(6,2));
        calendarPnl.add(optionPnl);

        JLabel bill = new JLabel("Billboard: ");
        optionPnl.add(bill);

        returnTitles();

        JLabel day = new JLabel("Day: ");
        optionPnl.add(day);
        dayList = new JList(days);
        dayList.setSelectedIndex(0);
        optionPnl.add(new JScrollPane(dayList));

        JLabel hour = new JLabel("Hour: ");
        optionPnl.add(hour);
        hourList = new JList(hours);
        hourList.setSelectedIndex(0);
        optionPnl.add(new JScrollPane(hourList));

        JLabel min = new JLabel("Minute: ");
        optionPnl.add(min);
        minList = new JList(minutes);
        minList.setSelectedIndex(0);
        optionPnl.add(new JScrollPane(minList));

        JLabel dur = new JLabel("Duration: ");
        optionPnl.add(dur);
        duration = new JTextField("1");
        optionPnl.add(duration);

        JLabel recur = new JLabel("Recur? ");
        optionPnl.add(recur);
        recurList = new JComboBox(recurStr);
        recurList.addActionListener(this);
        optionPnl.add(new JScrollPane(recurList));
    }

    /**
     * Sets up the window's GUI. Adds ActionListeners to the buttons.
     */
    public ScheduleGui() throws SQLException, IOException, ClassNotFoundException {
        setupGui();

        centerPane = new JLayeredPane();
        centerPane.setPreferredSize(new Dimension(50, 50));
        
        calendarPnl = new JPanel();
        calendarPnl.setLayout(new GridLayout(0,8));
        calendarPnl.setBounds(0,0,screenSize.width-60,screenSize.height-450);
        centerPane.add(calendarPnl);
        
        date = String.valueOf(today);
        weekSet(date);

        grabVar();
        //setupScheduleBillboard();


        setupOptions();
        mainPnl.add(centerPane);

        btnPnl.setPreferredSize(new Dimension(50, -550));
        btnPnl.setLayout(new FlowLayout(FlowLayout.LEFT, 10,15));
        mainPnl.add(btnPnl);

        backBtn.addActionListener(this);
        btnPnl.add(backBtn);

        JPanel filler = new JPanel();
        filler.setPreferredSize(new Dimension(1300, 50));
        filler.setBackground(Color.white);
        btnPnl.add(filler);

        scheduleBtn.addActionListener(this);
        btnPnl.add(scheduleBtn);
    }

    /**
     * Checks to see if a given string is a digit.
     * @param result The given string.
     * @return True if it is, false if it isn't.
     */
    private boolean checkDigits(String result) {
        if (!result.equals(null))
            for (int i = 0; i < result.length(); i++) {
                Character c = result.charAt(i);
                if (!(Character.isDigit(c))) return false;
            }
        return true;
    }

    /**
     * Finds the billboard ID for the given billboard (based on title).
     * @param title The title of the billboard to find.
     * @return The ID, or -1 if it could not be found.
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws IOException
     */
    private int getBillboardId(String title) throws ClassNotFoundException, SQLException, IOException {
        Socket socket = new Socket("localhost", 3129);
        ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());

        BillboardListRequest billboardListRequest = new BillboardListRequest();
        oos.writeObject(billboardListRequest);
        oos.flush();
        
        ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
        Object o = ois.readObject();

        Integer billId = -1;
        if (o instanceof BillboardListReply) {
            BillboardListReply billboardListReply = (BillboardListReply) o;
            ArrayList<String> billboardName = billboardListReply.getBillboardName();
            ArrayList<String> billboardId = billboardListReply.getBillboardId();
            
            for (int i = 0; i < billboardName.size(); i++) if (title.equals(billboardName.get(i)))
                billId = Integer.parseInt(billboardId.get(i));
            oos.flush();
        }

        if (billId < 0) return -1;
        else return billId;
    }

    /**
     * Schedules a billboard to be displayed at a given time. Configures whether or not the billboard will recur, and
     * how often it will if it does.
     * @param billboard The billboard to be scheduled.
     * @param dateStr The date of displaying, in string form.
     * @param totalMinutes The total number of minutes to display the billboard for.
     * @param time The duration the billboard will be displayed for.
     * @param recur Whether or not the billboard will be recurring.
     * @param recurMin If recurring every x minutes, the x value.
     * @throws IOException
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    private void scheduleBillboard(String billboard, String dateStr, String totalMinutes, String time, String recur,
                                   String recurMin) throws IOException, SQLException, ClassNotFoundException {
        int billId = getBillboardId(billboard);

        //"Tue, 2 Jun"
        String year = "2020";
        String day = "00";
        Pattern p = Pattern.compile("([0-9]+)");
        Matcher m = p.matcher(dateStr);
        if (m.find()) day = m.group(0);

        String month = dateStr.substring(7, 10);
        String monthSql = switch (month) {
            case "Jan" -> "01";
            case "Feb" -> "02";
            case "Mar" -> "03";
            case "Apr" -> "04";
            case "May" -> "05";
            case "Jun" -> "06";
            case "Jul" -> "07";
            case "Aug" -> "08";
            case "Sep" -> "09";
            case "Oct" -> "10";
            case "Nov" -> "11";
            case "Dec" -> "12";
            default -> "00";
        };

        String sqlFormat = year + "-" + monthSql + "-" + day;
        java.sql.Date current = java.sql.Date.valueOf(sqlFormat);

        int totalMin = Integer.parseInt(totalMinutes);
        int duration = Integer.parseInt(time);
        int recurInt = -1;
        // No recursion
        if (recur.equals(recurStr[0])) recurInt = 1;
        // Every day
        else if (recur.equals(recurStr[1])) recurInt = 2;
        // Every hour
        else if (recur.equals(recurStr[2])) recurInt = 3;
        // Every x minutes
        else if (recur.equals(recurStr[3])) recurInt = 4;

        int recurMinute = Integer.parseInt(recurMin);

        Socket socket = new Socket("localhost", 3129);
        ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());

        ScheduleAddRequest scheduleClass = new ScheduleAddRequest(billId, current, totalMin, duration, recurInt, recurMinute);

        oos.writeObject(scheduleClass);
        oos.flush();

        oos.close();
        socket.close();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object src = e.getSource();

        if (src == backBtn) {
            MainGui main = new MainGui();
            scheduleFrame.setVisible(false);

        } else if (src == scheduleBtn) {
            String billboard = (String) billList.getSelectedValue();
            String day = (String) dayList.getSelectedValue();
            String hour = (String) hourList.getSelectedValue();
            String minute = (String) minList.getSelectedValue();
            String time = duration.getText();
            String recur = (String) recurList.getSelectedItem();
            String recurMin;

            int totalMinutes = Integer.parseInt(minute) + (Integer.parseInt(hour)*60);

            String [] serverString = new String[7];
            serverString[0]= "0"; //NEED TO IMPLEMENT ID NUMBER
            //Serve server for billboard id with same title name
            //if nothing return error
            serverString[1] = billboard;
            serverString[2] = day;
            serverString[3] = Integer.toString(totalMinutes);
            serverString[5] = recur;

            if (checkDigits(time) && Integer.parseInt(time) > 0) {
                if (recur.equals("Every _ Minutes")) {
                    String result = (String) JOptionPane.showInputDialog(scheduleFrame,
                            "How often should the billboard recur?", "Minutes to Recur",
                            JOptionPane.PLAIN_MESSAGE, null, null, "1");

                    if (checkDigits(result) && Integer.parseInt(result) > 0 && Integer.parseInt(result) >
                            Integer.parseInt(time)) {
                        recurMin = result;
                        serverString[4] = time;
                        serverString[6] = recurMin;

                        try {
                            scheduleBillboard(serverString[1], serverString[2], serverString[3], serverString[4],
                                    serverString[5], serverString[6]);
                        } catch (IOException | SQLException | ClassNotFoundException ioException) {
                            ioException.printStackTrace();
                        }

                        JOptionPane.showMessageDialog(scheduleFrame,
                                "Billboard has been successfully scheduled.");

                        scheduleFrame.setVisible(false);
                        try {
                            ScheduleGui main = new ScheduleGui();

                        } catch (SQLException throwables) {
                            throwables.printStackTrace();
                        } catch (IOException ioException) {
                            ioException.printStackTrace();
                        } catch (ClassNotFoundException classNotFoundException) {
                            classNotFoundException.printStackTrace();
                        }
                    } else {
                        JOptionPane.showMessageDialog(scheduleFrame, "The time the billboard recurs must only " +
                                "have digits and must be longer than the duration of the billboard.",
                                "Recur Minute Error", JOptionPane.ERROR_MESSAGE);
                    }
                }
                else {
                    serverString[4] = time;
                    serverString[6] = "0";

                    int total = 25*60;
                    int timeLeft = (Integer.parseInt(hour) *60) + Integer.parseInt(minute);
                    int hourLeft = timeLeft + Integer.parseInt(time);

                    if (hourLeft <= total) {
                        try {
                            scheduleBillboard(serverString[1],  serverString[2], serverString[3], serverString[4],
                                    serverString[5], serverString[6]);
                        } catch (IOException | SQLException | ClassNotFoundException ioException) {
                            ioException.printStackTrace();
                        }
                        JOptionPane.showMessageDialog(scheduleFrame, "Billboard has been successfully scheduled.");
                        scheduleFrame.setVisible(false);
                        try {

                            ScheduleGui main = new ScheduleGui();

                        } catch (SQLException throwables) {
                            throwables.printStackTrace();
                        } catch (IOException ioException) {
                            ioException.printStackTrace();
                        } catch (ClassNotFoundException classNotFoundException) {
                            classNotFoundException.printStackTrace();
                        }

                    } else {
                        JOptionPane.showMessageDialog(scheduleFrame, "The billboard duration must be less " +
                                        "than the time left in the day or start earlier.", "Duration Error",
                                JOptionPane.ERROR_MESSAGE);
                    }

                }
            }
            else JOptionPane.showMessageDialog(scheduleFrame, "The billboard duration must be only in digits " +
                        "and has to be 1 minute or longer.", "Duration Error", JOptionPane.ERROR_MESSAGE);
        }
    }
}
