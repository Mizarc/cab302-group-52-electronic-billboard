package org.cab302.group52;

import org.cab302.group52.networkdata.billboard.*;
import org.w3c.dom.Attr;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.Socket;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Creates the GUI used for creating billboards. Adds any created billboards to the server when requested.
 */
public class CreateGui extends JFrame implements ActionListener {
    JButton createBtn = StyleGuide.setupBtn("Create", "Medium"),
            backBtn = StyleGuide.setupBtn("Back", "Medium"),
            previewBtn = StyleGuide.setupBtn("Preview", "Medium");
    JFrame createFrame = StyleGuide.setupMainFrame("Create Billboard", 700, 900, false, true);
    JPanel mainPnl = StyleGuide.setupPanel(), btnPnl = StyleGuide.setupPanel(), previewPnl = StyleGuide.setupPanel(),
            billboardPnl = StyleGuide.setupPanel(), imagePnl;
    JPanel optionPnl;
    JTextField titleText, messageText,
            pictureText, infoText;
    JComboBox cbColour1, cbColour2, cbColour3;

    static Boolean isOptionsAlreadyCreated = false;

    /**
     * Adds the text labels and other widgets to the window. Marks the option window created boolean as true to prevent
     * the options from being readded to the panel.
     */
    private void addLabels() {
        optionPnl = StyleGuide.setupPanel();
        GridLayout optionGrid = new GridLayout(25, 0);
        optionGrid.setVgap(15);
        optionPnl.setLayout(optionGrid);
        optionPnl.setPreferredSize(new Dimension(300, 900));
        billboardPnl.add(optionPnl);

        JLabel titleLabel = StyleGuide.setupLabel("Title: ", "Small");
        optionPnl.add(titleLabel);
        titleText = StyleGuide.setupTField();
        optionPnl.add(titleText);

        JLabel backgroundLabel = StyleGuide.setupLabel("Background Colour: ", "Small");
        optionPnl.add(backgroundLabel);
        String type = "background";
        cbColour1 = ColorComboRenderer.ComboBox(cbColour1, type);
        optionPnl.add(cbColour1);

        JLabel messageLabel = StyleGuide.setupLabel("Message: ", "Small");
        optionPnl.add(messageLabel);
        messageText = StyleGuide.setupTField();
        optionPnl.add(messageText);

        JLabel colourLabel = StyleGuide.setupLabel("Message Colour: ", "Small");
        optionPnl.add(colourLabel);
        String type2 = "font";
        cbColour2 = ColorComboRenderer.ComboBox(cbColour2, type2);
        optionPnl.add(cbColour2);

        JLabel pictureLabel = StyleGuide.setupLabel("Picture: ", "Small");
        optionPnl.add(pictureLabel);
        pictureText = StyleGuide.setupTField();
        optionPnl.add(pictureText);

        JLabel infoLabel = StyleGuide.setupLabel("Information: ", "Small");
        optionPnl.add(infoLabel);
        infoText = StyleGuide.setupTField();
        optionPnl.add(infoText);

        JLabel colourLabel2 = StyleGuide.setupLabel("Information Colour: ", "Small");
        optionPnl.add(colourLabel2);
        cbColour3 = ColorComboRenderer.ComboBox(cbColour3, type2);
        optionPnl.add(cbColour3);

        isOptionsAlreadyCreated = true;
    }

    /**
     * Sets up the window's main frame, panel, and title.
     */
    private void setupGui() {
        mainPnl.setLayout(new BoxLayout(mainPnl, BoxLayout.PAGE_AXIS));
        createFrame.add(mainPnl);

        JPanel titlePnl = StyleGuide.setupPanel();
        titlePnl.setPreferredSize(new Dimension(900, -550));
        titlePnl.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 0));
        mainPnl.add(titlePnl);
        JLabel title = StyleGuide.setupTitle("Create Billboard", "Large");
        titlePnl.add(title);
    }

    /**
     * Sets up the window's GUI. Adds ActionListeners to the buttons.
     */
    public CreateGui() {
        super("Create GUI");
        setupGui();

        billboardPnl.setLayout(new FlowLayout(FlowLayout.CENTER, 35, 5));
        billboardPnl.setPreferredSize(new Dimension(300, -50));
        mainPnl.add(billboardPnl);

        addLabels();

        btnPnl.setPreferredSize(new Dimension(50, -550));
        btnPnl.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 15));
        mainPnl.add(btnPnl);
        backBtn.addActionListener(this);
        btnPnl.add(backBtn);
        previewBtn.addActionListener(this);
        btnPnl.add(previewBtn);
        createBtn.addActionListener(this);
        btnPnl.add(createBtn);
    }

    /**
     * Takes the values given by the user and creates a new billboard on the server as an XML file.
     * @param user The user creating the billboard.
     * @param billboardName The name to be assigned to the new billboard.
     * @param message The message to be assigned to the new billboard.
     * @param picture The picture to be assigned to the new billboard.
     * @param info The information text to be assigned to the new billboard.
     * @param colourBillStr The colour of the new billboard to be assigned.
     * @param colourMsgStr The colour of the new billboard's messaged to be assigned.
     * @param colourInfoStr The colour of the new billboard's information text to be assigned.
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws TransformerException
     */
    private static void SendBillboard(String user, String billboardName, String message, String picture,
                                    String info, String colourBillStr, String  colourMsgStr, String colourInfoStr)
                                    throws IOException, ParserConfigurationException, TransformerException {
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.newDocument();

            Element rootElement = doc.createElement("billboard");
            doc.appendChild(rootElement);

            Attr billAttr = doc.createAttribute("background");
            billAttr.setValue(colourBillStr);
            rootElement.setAttributeNode(billAttr);

            Element messageEle = doc.createElement("message");
            messageEle.appendChild(doc.createTextNode(message));
            rootElement.appendChild(messageEle);

            Attr msgAttr = doc.createAttribute("colour");
            msgAttr.setValue(colourMsgStr);
            messageEle.setAttributeNode(msgAttr);

            Element pictureEle = doc.createElement("picture");
            rootElement.appendChild(pictureEle);

            if (picture.contains("http")) {
                Attr picAttr = doc.createAttribute("url");
                picAttr.setValue(picture);
                pictureEle.setAttributeNode(picAttr);
            } else {
                Attr picAttr = doc.createAttribute("data");
                picAttr.setValue(picture);
                pictureEle.setAttributeNode(picAttr);
            }

            Element infoEle = doc.createElement("information");
            infoEle.appendChild(doc.createTextNode(info));
            rootElement.appendChild(infoEle);

            Attr infoAttr = doc.createAttribute("colour");
            infoAttr.setValue(colourInfoStr);
            infoEle.setAttributeNode(infoAttr);

            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");

            //Convert to XML to string
            StringWriter sw = new StringWriter();
            StreamResult result = new StreamResult(sw);
            DOMSource source = new DOMSource(doc);
            transformer.transform(source, result);
            String xmlString = sw.toString();

            Socket socket = new Socket("localhost", 3129);

            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());

            BillboardAddRequest billboardClass = new BillboardAddRequest(user, billboardName, xmlString);

            oos.writeObject(billboardClass);
            oos.flush();

            oos.close();
            socket.close();
        } catch (ParserConfigurationException | DOMException | IllegalArgumentException | TransformerException |
                IOException | TransformerFactoryConfigurationError e) {
            e.printStackTrace();
        }
    }

    /**
     * Checks the server to see if a billboard with the given title already exists.
     * @param title The title to find.
     * @return True if it does, false if it doesn't.
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws IOException
     */
    private boolean checkTitle(String title) throws ClassNotFoundException, SQLException, IOException {
        Socket socket = new Socket("localhost", 3129);
        ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());

        BillboardListRequest billboardListRequest = new BillboardListRequest();
        oos.writeObject(billboardListRequest);
        oos.flush();

        ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
        Object o = ois.readObject();

        if (o instanceof BillboardListReply) {
            BillboardListReply billboardListReply = (BillboardListReply) o;
            ArrayList<String> billboardName = billboardListReply.getBillboardName();

            for (int i = 0; i < billboardName.size(); i++) if (title.equals(billboardName.get(i))) return true;
            oos.flush();
        }
        return false;
    }

    boolean isThere = false;

    @Override
    public void actionPerformed(ActionEvent e) {
        Object src = e.getSource();

        if (src == backBtn) {
            MainGui main = new MainGui();
            createFrame.setVisible(false);
        }
        else if (src == previewBtn) {
            String message = messageText.getText();
            String picture = pictureText.getText();
            String info = infoText.getText();
            Object colourBillboard = cbColour1.getItemAt(cbColour1.getSelectedIndex());
            String colourBillStr = colourBillboard.toString();
            Object colourMsg = cbColour2.getItemAt(cbColour2.getSelectedIndex());
            String colourMsgStr = colourMsg.toString();
            Object colourInfo = cbColour3.getItemAt(cbColour3.getSelectedIndex());
            String colourInfoStr = colourInfo.toString();

            try {
                SetupBillboard previewWindow = new SetupBillboard(true, colourBillStr, message, colourMsgStr,
                        picture, info, colourInfoStr);
            } catch (InterruptedException interruptedException) {
                interruptedException.printStackTrace();
            }
        }
        else if (src == createBtn) {
            String billboardName = titleText.getText();
            String message = messageText.getText();
            String picture = pictureText.getText();
            String info = infoText.getText();
            Object colourBillboard = cbColour1.getItemAt(cbColour1.getSelectedIndex());
            String colourBillStr = colourBillboard.toString();
            Object colourMsg = cbColour2.getItemAt(cbColour2.getSelectedIndex());
            String colourMsgStr = colourMsg.toString();
            Object colourInfo = cbColour3.getItemAt(cbColour3.getSelectedIndex());
            String colourInfoStr = colourInfo.toString();

            try {
                isThere = checkTitle(billboardName);
                if (!isThere) {
                    SendBillboard(LoginGui.userName, billboardName, message, picture, info, colourBillStr, colourMsgStr,
                            colourInfoStr);
                    JOptionPane.showMessageDialog(createFrame, "Billboard has successfully been added to server.");
                    isThere = false;
                }
                else JOptionPane.showMessageDialog(createFrame, "Billboard title already exists in " +
                        "database, please change to a unique title", "Unique Title Error",
                        JOptionPane.ERROR_MESSAGE);

            } catch (Exception ex) {
                System.out.println("error   ");
            }
        }
    }
}
