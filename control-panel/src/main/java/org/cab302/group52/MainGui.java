package org.cab302.group52;

import org.cab302.group52.networkdata.*;
import org.cab302.group52.CreateGui.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.sql.SQLException;

/**
 * Acts as a menu from which the user can select
 */
public class MainGui extends JFrame implements ActionListener {
    JButton createBtn = StyleGuide.setupBtn("Create", "Large"),
            listBtn = StyleGuide.setupBtn("List", "Large"),
            modifyBtn = StyleGuide.setupBtn("Modify", "Large"),
            scheduleBtn = StyleGuide.setupBtn("Schedule", "Large"),
            userBtn = StyleGuide.setupBtn("Edit Users", "Large"),
            changeBtn = StyleGuide.setupBtn("Change Password", "Large"),
            logOutBtn = StyleGuide.setupBtn("Log Out", "Small");
    JFrame mainFrame = StyleGuide.setupMainFrame("Control Panel", 600, 850, false, true);
    JPanel btnPnl;
    static String[] permissions = new String[] {"", "", "", ""};
    
    /**
     * Initialises the List button.
     */
    private void ListBtn() {
        listBtn.addActionListener(this);
        btnPnl.add(listBtn);
    }

    /**
     * Initialises the Create button.
     */
    private void CreateBtn() {
        createBtn.addActionListener(this);
        btnPnl.add(createBtn);
    }

    /**
     * Initialises the Modify button.
     */
    private void ModifyBtn() {
        modifyBtn.addActionListener(this);
        btnPnl.add(modifyBtn);
    }

    /**
     * Initialises the Schedule button.
     */
    private void ScheduleBtn() {
        scheduleBtn.addActionListener(this);
        btnPnl.add(scheduleBtn);
    }

    /**
     * Initialises the Edit Users button.
     */
    private void EditBtn() {
        userBtn.addActionListener(this);
        btnPnl.add(userBtn);
    }

    /**
     * Initialises the Change Password button.
     */
    private void ChangeBtn() {
        changeBtn.addActionListener(this);
        btnPnl.add(changeBtn);
    }

    /**
     * Defines the available permissions for the current user.
     * @param create Whether or not the current user has the Create permission.
     * @param modify Whether or not the current user has the Modify permission.
     * @param schedule Whether or not the current user has the Schedule permission.
     * @param edit Whether or not the current user has the Edit permission.
     */
    static public void setPermissions(String create, String modify, String schedule, String edit){
        if (create.equals("1")) permissions[0] = "Create";
        else permissions[0] = "";

        if (modify.equals("1")) permissions[1] = "Modify";
        else permissions[1] = "";

        if (schedule.equals("1")) permissions[2] = "Schedule";
        else permissions[2] = "";

        if (edit.equals("1")) permissions[3] = "EditUsers";
        else permissions[3] = "";
    }
    
    /**
     * Sets up the window's GUI. Checks a user's permissions before adding buttons, as some users do not have access
     * to every function of the control panel. Adds only the appropriate buttons.
     */
    public MainGui() {
        super("MainGui");

        JPanel mainPnl =  new JPanel();
        mainPnl.setLayout(new BoxLayout(mainPnl, BoxLayout.PAGE_AXIS));
        mainFrame.add(mainPnl);

        JPanel titlePnl = StyleGuide.setupPanel();
        titlePnl.setPreferredSize(new Dimension(900, -350));
        mainPnl.add(titlePnl);

        JLabel title = StyleGuide.setupTitle("Control Panel", "Large");
        titlePnl.add(title);

        btnPnl = new JPanel();
        btnPnl.setLayout(new FlowLayout(FlowLayout.CENTER, 5,10));
        btnPnl.setBackground(Color.white);
        mainPnl.add(btnPnl);

        ListBtn();
        for (int i = 0; i < permissions.length; i++)
            switch (permissions[i]) {
                case "Create" -> CreateBtn();
                case "Modify" -> ModifyBtn();
                case "Schedule" -> ScheduleBtn();
                case "EditUsers" -> EditBtn();
            }
        ChangeBtn();

        JPanel logPnl = StyleGuide.setupPanel();
        logPnl.setPreferredSize(new Dimension(50, -350));
        mainPnl.add(logPnl);

        logOutBtn.addActionListener(this);
        logPnl.add(logOutBtn);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object src = e.getSource();
        if (src == listBtn) {
            try {
                ListBillboard main = new ListBillboard();
            } catch (IOException | ClassNotFoundException | SQLException ioException) {
                ioException.printStackTrace();
            }
            mainFrame.setVisible(false);
        }
        else if (src == createBtn) {
            CreateGui main = new CreateGui();
            mainFrame.setVisible(false);

        } else if(src==modifyBtn){
            try {

                ModifyGui main = new ModifyGui();

            } catch (IOException ioException) {
                ioException.printStackTrace();
            } catch (ClassNotFoundException classNotFoundException) {
                classNotFoundException.printStackTrace();
            }
            mainFrame.setVisible(false);
        }
        else if (src == scheduleBtn) {
            try {
                ScheduleGui main = new ScheduleGui();
            } catch (SQLException | ClassNotFoundException | IOException throwables) {
                throwables.printStackTrace();
            }
            mainFrame.setVisible(false);
        }
        else if (src == userBtn) {
            try {
                UsersGui main = new UsersGui();
            } catch (IOException | ClassNotFoundException ioException) {
                ioException.printStackTrace();
            }
            mainFrame.setVisible(false);
        }
        else if (src == changeBtn) {
            ChangePassGui main = new ChangePassGui();
            mainFrame.setVisible(false);
        }
        else if (src == logOutBtn){
            LoginGui login = new LoginGui();
            permissions = new String[4];
            LoginGui.userName = "";
            mainFrame.setVisible(false);
        }


    }
}
