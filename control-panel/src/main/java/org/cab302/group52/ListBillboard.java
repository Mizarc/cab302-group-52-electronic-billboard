package org.cab302.group52;

import org.cab302.group52.networkdata.billboard.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.*;
import java.net.Socket;
import java.sql.*;
import java.util.ArrayList;

/**
 * Creates a window that lists every billboard currently available in the server.
 */
public class ListBillboard extends JFrame implements ActionListener {

    JFrame mainFrame = StyleGuide.setupMainFrame("List Billboards", 1100, 900, false, true);
    JTable table;
    JPanel btnPnl = StyleGuide.setupPanel(), listPnl = StyleGuide.setupPanel(),
            mainPnl = StyleGuide.setupPanel();
    JButton backBtn = StyleGuide.setupBtn("Back", "Medium"),
            previewBtn = StyleGuide.setupBtn("Preview", "Medium"),
            editBtn = StyleGuide.setupBtn("Modify", "Medium"),
            deleteBtn = StyleGuide.setupBtn("Delete", "Medium");
    static String[] data = null;
    static Boolean fromList = false;

    /**
     * Sets up the window's main frame, panel, and title.
     */
    private void listGuiSetup() {
        mainPnl.setLayout(new BoxLayout(mainPnl, BoxLayout.PAGE_AXIS));
        mainFrame.add(mainPnl);

        JPanel titlePnl = StyleGuide.setupPanel();
        titlePnl.setPreferredSize(new Dimension(900, -450));
        mainPnl.add(titlePnl);
        JLabel title = StyleGuide.setupTitle("Billboards Currently in the System", "Medium");
        titlePnl.add(title);
    }

    /**
     * Sets up the window's GUI. Adds ActionListeners to the buttons.
     */
    public ListBillboard() throws IOException, ClassNotFoundException, SQLException {
        super("ListBillboard");
        listGuiSetup();

        listPnl.setPreferredSize(new Dimension(1000, 50));
        mainPnl.add(listPnl);

        loadDatabase();

        btnPnl.setLayout(new FlowLayout(FlowLayout.LEFT, 25,25));
        btnPnl.setPreferredSize(new Dimension(900, -450));
        mainPnl.add(btnPnl);
        backBtn.addActionListener(this);
        btnPnl.add(backBtn);
        previewBtn.addActionListener(this);
        btnPnl.add(previewBtn);
        editBtn.addActionListener(this);
        btnPnl.add(editBtn);
        deleteBtn.addActionListener(this);
        btnPnl.add(deleteBtn);
    }

    /**
     * Loads the database of billboards from the server, and formats them as a table. This table is not editable by
     * the user, and it can be scrolled through. When a user selects a row in the table, its data is extracted (to be
     * used with the Preview or Modify functions).
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws IOException
     */
    void loadDatabase() throws ClassNotFoundException, SQLException, IOException {
        boolean loadDatabase = true;
        Socket socket = new Socket("localhost", 3129);
        ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());

        BillboardListRequest billboardListRequest = new BillboardListRequest();
        oos.writeObject(billboardListRequest);
        oos.flush();

        ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
        Object o = ois.readObject();

        if (o instanceof BillboardListReply) {
            BillboardListReply billboardListReply = (BillboardListReply) o;
            ArrayList<String> billboardId = billboardListReply.getBillboardId();
            ArrayList<String> user = billboardListReply.getUser();
            ArrayList<String> billboardName = billboardListReply.getBillboardName();

            String[] columnNames = {"Billboard ID", "Name","Created by"};
            String[][] finalData = new String[billboardId.size()][3];

            for (int i = 0; i < billboardId.size(); i++) {
                finalData[i][0] = billboardId.get(i);
                finalData[i][1] = billboardName.get(i);
                finalData[i][2] = user.get(i);
            }

            DefaultTableModel tableModel = new DefaultTableModel(finalData, columnNames) {
                @Override
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };

            table = new JTable(finalData, columnNames);
            table.setModel(tableModel);
            table.setCellSelectionEnabled(true);
            JScrollPane scrollP = new JScrollPane(table);
            listPnl.add(scrollP);

            table.addMouseListener(new MouseAdapter() {
                public void mouseClicked(MouseEvent e) {
                    data = new String[2];
                    int selectedRow = table.getSelectedRow();
                    data[0] = table.getValueAt(selectedRow, 0).toString();
                    data[1] = table.getModel().getValueAt(selectedRow, 1).toString();
                    System.out.println(data[0]);
                }
            });
            oos.flush();
        }
    }

    /**
     * Parses a billboard XML file to be previewed in the application.
     * @param id The ID of the requested billboard.
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void PreviewXML(String id) throws IOException, ClassNotFoundException, InterruptedException {
        String billColor = "", message = "", msgColor = "", picture = "", info = "", infoColor = "";

        Socket socket = new Socket("localhost", 3129);
        OutputStream outputStream = socket.getOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
        BillboardXMLRequest billboardXMLrequest = new BillboardXMLRequest(id);
        oos.writeObject(billboardXMLrequest);
        oos.flush();
        ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
        Object o = ois.readObject();

        if (o instanceof BillboardXMLReply){
            BillboardXMLReply billboardXMLReply = (BillboardXMLReply) o;
            String xml = billboardXMLReply.getXML();

            try {
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db;
                db = dbf.newDocumentBuilder();
                Document doc = db.parse(new InputSource(new StringReader(xml)));
                doc.getDocumentElement().normalize();
                Element root = doc.getDocumentElement();

                billColor = root.getAttribute("background");
                message = root.getElementsByTagName("message").item(0).getTextContent();

                NamedNodeMap msgNM = root.getElementsByTagName("message").item(0).getAttributes();
                Node col = msgNM.getNamedItem("colour");
                msgColor = col.toString();

                NamedNodeMap picNM = root.getElementsByTagName("picture").item(0).getAttributes();
                Node urlNode = picNM.getNamedItem("url");
                if (urlNode == null) {
                    Node dataNode = picNM.getNamedItem("data");
                    picture = dataNode.toString();
                    picture = SetupBillboard.findPictureData(picture);
                }
                else {
                    picture = urlNode.toString();
                    picture = SetupBillboard.findPictureData(picture);
                }
                info = root.getElementsByTagName("information").item(0).getTextContent();
                NamedNodeMap infoNM = root.getElementsByTagName("information").item(0).getAttributes();
                Node infoCol= infoNM.getNamedItem("colour");
                infoColor = infoCol.toString();
            } catch (Exception e) { }
            oos.flush();
            SetupBillboard previewWindow = new SetupBillboard(true, billColor, message, msgColor,
                    picture, info, infoColor);
        }
    }

    /**
     * Deletes the selected billboard from the server.
     * @param id The billboard to delete.
     * @throws IOException
     */
    private void deleteSelected(String id) throws IOException {
        Socket socket = new Socket("localhost", 3129);
        OutputStream outputStream = socket.getOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
        BillboardRemoveRequest billboardID = new BillboardRemoveRequest(id);
        oos.writeObject(billboardID);
        oos.writeUTF("billboardID");
        System.out.println(billboardID);
        oos.flush();
        socket.close();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object src = e.getSource();

        if (src==backBtn) {
            mainFrame.setVisible(false);
            MainGui main = new MainGui();
        }

        else if (src == previewBtn) {
            try {
                int selectedRow = table.getSelectedRow();
                String id =  table.getValueAt(selectedRow, 0).toString();
                PreviewXML(id);
            } catch (Exception exception) { exception.printStackTrace(); }
        }

        else if (src == editBtn) {
            if (MainGui.permissions[1].equals("Modify")) {
                // fromList- restricts admin from selecting other billboards while in modify gui
                // To select billboards the admin must enter modify gui from the main menu
                fromList = true;
                try {

                    ModifyGui main = new ModifyGui();

                } catch (IOException ioException) {
                    ioException.printStackTrace();
                } catch (ClassNotFoundException classNotFoundException) {
                    classNotFoundException.printStackTrace();
                }
                mainFrame.setVisible(false);

            } else if(MainGui.permissions[0] == "Create" && MainGui.permissions[1] == "") {

                //If billboard is owned by user
                if(data[1].matches(LoginGui.userName)) {

                    try {

                        ModifyGui main = new ModifyGui();

                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    } catch (ClassNotFoundException classNotFoundException) {
                        classNotFoundException.printStackTrace();
                    }
                    mainFrame.setVisible(false);
                }
                else JOptionPane.showMessageDialog(mainFrame, "Permission denied. Billboard is not owned by user.",
                        "Edit Request Error", JOptionPane.ERROR_MESSAGE);
            }
            else JOptionPane.showMessageDialog(mainFrame, "Permission denied. User cannot edit any billboards.",
                    "Edit Request Error", JOptionPane.ERROR_MESSAGE);
        }

        else if (src == deleteBtn) {
            try {
                int selectedRow = table.getSelectedRow();
                String id =  data[0] = table.getValueAt(selectedRow, 0).toString();
                deleteSelected(id);
                ListBillboard main = new ListBillboard();
                mainFrame.setVisible(false);
            } catch (IOException | SQLException | ClassNotFoundException ioException) {
                ioException.printStackTrace();
            }
            if (MainGui.permissions[1].equals("Modify")) System.out.println("Delete billboard - as admin");
            else if (MainGui.permissions[0].equals("Create") && MainGui.permissions[1].equals("")) {
                if (data[1].matches(LoginGui.userName)) System.out.println("User deletes own billboard");
                else JOptionPane.showMessageDialog(mainFrame, "Permission denied. Billboard is not owned by user.",
                        "Delete Request Error", JOptionPane.ERROR_MESSAGE);
            }
            else JOptionPane.showMessageDialog(mainFrame, "Permission denied. User cannot delete any billboards.",
                        "Delete Request Error", JOptionPane.ERROR_MESSAGE);
        }
    }
}

