package org.cab302.group52;

import org.cab302.group52.networkdata.user.*;

import java.awt.*;
import java.awt.desktop.SystemSleepEvent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.swing.*;

/**
 * Creates the window that allows the user to log in. Handles login management and authentication.
 */
public class LoginGui extends JFrame implements ActionListener {

    JTextField userTxt = StyleGuide.setupTField();
    JTextField passTxt = StyleGuide.setupPField();
    JFrame loginFrame = StyleGuide.setupMainFrame("Login Authentication", 350, 400, false,
            false);
    static String userName = "";

    /**
     * Sets up the window's GUI. Adds ActionListeners to the buttons. Establishes the Enter key as an alternative to
     * the log in button.
     */
    public LoginGui() {
        super("Login Authentication");

        GridLayout mainLayout = new GridLayout(4,0);
        JPanel mainPnl = StyleGuide.setupPanel();
        mainPnl.setLayout(mainLayout);
        loginFrame.add(mainPnl);

        GridLayout topLayout = new GridLayout(1,0);
        JPanel labPnl = StyleGuide.setupPanel();
        labPnl.setLayout(topLayout);
        mainPnl.add(labPnl);

        JPanel userPnl = StyleGuide.setupPanel();
        mainPnl.add(userPnl);
        JPanel passPnl = StyleGuide.setupPanel();
        mainPnl.add(passPnl);
        JPanel botPnl = StyleGuide.setupPanel();
        mainPnl.add(botPnl);

        JLabel log = StyleGuide.setupTitle("Welcome!", "Small");
        log.setBounds(90,100,300,20);
        labPnl.add(log);

        JLabel userName = StyleGuide.setupLabel("Username: ", JLabel.LEFT, JLabel.CENTER, "Small");
        userPnl.add(userName);
        userPnl.add(userTxt);

        JLabel passWord = StyleGuide.setupLabel("Password: ", JLabel.LEFT, JLabel.CENTER, "Small");
        passPnl.add(passWord);
        passPnl.add(passTxt);

        JButton loginBtn = StyleGuide.setupBtn("Log In", "Small");
        loginBtn.addActionListener(this);
        botPnl.add(loginBtn);

        KeyStroke enter = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0);
        mainPnl.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(enter, "LogIn");
        mainPnl.getActionMap().put("LogIn", LogIn);

        loginFrame.setVisible(true);
    }

    /**
     * Converts hashes to strings. Passwords are stored as hashes, but must be converted to strings before they can be
     * changed.
     * @param hash The hash to convert.
     * @return A hash as a string.
     */
    private static String byteToString(byte[] hash) {
        StringBuffer sb = new StringBuffer();
        for(byte b : hash) sb.append(String.format("%02x",b & 0xFF));
        return sb.toString();
    }

    /**
     * Takes the given username and password, and connects to the server to make sure those details are valid.
     * @throws NoSuchAlgorithmException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void loginValidation() throws NoSuchAlgorithmException, IOException, ClassNotFoundException {
        Socket socket = new Socket("localhost", 3129);
        String passwordLogin = passTxt.getText();
        String username = userTxt.getText();

        if(username.isEmpty() && passwordLogin.isEmpty()) JOptionPane.showMessageDialog(this,
                "Please enter username and password.", "Login error", JOptionPane.ERROR_MESSAGE);
        else if (username.contains("") && passwordLogin.contains("")) {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] hashedPassword = md.digest(passwordLogin.getBytes());
            String hashedpassword = byteToString(hashedPassword);
            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());

            LoginRequest loginRequest = new LoginRequest(username, hashedpassword);
            oos.writeObject(loginRequest);
            oos.flush();
        }
        ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
        Object o = ois.readObject();

        if (o instanceof LoginReply) {
            LoginReply loginRequestReply = (LoginReply) o;

            if (loginRequestReply.isSuccessful()) {
                System.out.println("Successfully logged in, received session token " + loginRequestReply.getSessionToken());
                userName = loginRequestReply.getUser();
                String createPermission = loginRequestReply.getCreatePermission();
                String editBillboardPermission = loginRequestReply.getEditBillboardPermission();
                String schedulePermission = loginRequestReply.getSchedulePermission();
                String editUsers = loginRequestReply.getEditUsersPermission();
                MainGui.setPermissions(createPermission, editBillboardPermission, schedulePermission, editUsers);
                MainGui main = new MainGui();
                loginFrame.setVisible(false);
                ois.close();
            } else if (!loginRequestReply.isSuccessful()) {
                JOptionPane.showMessageDialog(this, "Invalid username or password.",
                        "Login error", JOptionPane.ERROR_MESSAGE);
                ois.close();
            }
        }
        socket.close();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            loginValidation();
        } catch (NoSuchAlgorithmException | IOException | ClassNotFoundException noSuchAlgorithmException) {
            noSuchAlgorithmException.printStackTrace();
        }
    }

    Action LogIn = new AbstractAction() {
        public void actionPerformed(ActionEvent e) {
            try {
                loginValidation();
            } catch (NoSuchAlgorithmException | IOException | ClassNotFoundException noSuchAlgorithmException) {
                noSuchAlgorithmException.printStackTrace();
            }
        }
    };
}
