package org.cab302.group52;

import org.cab302.group52.networkdata.user.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Creates the Change Password window, which allows the current user to change their own password.
 */
public class ChangePassGui extends JFrame implements ActionListener {

    JFrame changeFrame = StyleGuide.setupMainFrame("Edit Users", 600, 800, false, true);
    JPanel mainPnl = StyleGuide.setupPanel(), btnPnl = StyleGuide.setupPanel(),
            midPnl = StyleGuide.setupPanel(), titlePnl = StyleGuide.setupPanel();
    JButton backBtn = StyleGuide.setupBtn("Back", "Medium"),
            changeBtn = StyleGuide.setupBtn("Update Password", "Large");
    JLabel title = StyleGuide.setupTitle("Change Password", "Large");
    JLabel usernameLabel = StyleGuide.setupLabel("Username: ", "Small");
    JTextField usernameText = StyleGuide.setupPField();
    JLabel oldPassLabel = StyleGuide.setupLabel("Old Password: ", "Small");
    JTextField oldPassText = StyleGuide.setupPField();
    JLabel newPassLabel = StyleGuide.setupLabel("Password: ", "Small");
    JTextField newPassText = StyleGuide.setupPField();

    /**
     * Adds the text labels to the window.
     */
    private void textLabels() {
        midPnl.add(usernameLabel);
        midPnl.add(usernameText);

        midPnl.add(oldPassLabel);
        midPnl.add(oldPassText);

        midPnl.add(newPassLabel);
        midPnl.add(newPassText);
    }

    /**
     * Adds the ActionListener to the Change Password button.
     */
    private void changeBtn(){
        changeBtn.addActionListener(this);
    }

    /**
     * Basic setup for the GUI. Creates the main panel, the title panel, and adds the title to the panel.
     */
    private void setupGui() {
        BorderLayout layout = new BorderLayout();
        layout.setVgap(50);
        mainPnl.setLayout(layout);
        changeFrame.add(mainPnl);
        mainPnl.add(titlePnl, BorderLayout.NORTH);
        titlePnl.add(title);
    }

    /**
     * Initialises the GUI, adds panels for text, text fields, buttons, and filler. Fills said panels.
     * Creates ActionListeners for the Back and Update Password buttons.
     */
    public ChangePassGui() {
        setupGui();

        StyleGuide.setupFillerPanels(mainPnl);
        midPnl.setLayout(new FlowLayout(FlowLayout.CENTER, 20,30));
        mainPnl.add(midPnl, BorderLayout.CENTER);

        textLabels();

        btnPnl.setLayout(new FlowLayout(FlowLayout.CENTER, 5,15));
        mainPnl.add(btnPnl, BorderLayout.SOUTH);
        backBtn.addActionListener(this);
        btnPnl.add(backBtn);
        changeBtn.addActionListener(this);
        btnPnl.add(changeBtn);
        changeBtn();
    }

    /**
     * Converts hashes to strings. Passwords are stored as hashes, but must be converted to strings before they can be
     * changed.
     * @param hash The hash to convert.
     * @return A hash as a string.
     */
    private static String byteToString(byte[] hash){
        StringBuffer sb = new StringBuffer();
        for(byte b : hash) sb.append(String.format("%02x",b & 0xFF));
        return sb.toString();
    }

    /**
     * Changes a user's password.
     * @param hashedOldpassword The old password in hash form.
     * @param hashedNewPassword The new password in hash form.
     * @param userName The username for the account that's having its password changed.
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void ChangePassword(String hashedOldpassword, String hashedNewPassword, String userName)
            throws IOException, ClassNotFoundException {
        Socket socket = new Socket("localhost", 3129);
        String username = usernameText.getText();
        String oldp  = oldPassText.getText();
        String newp = newPassText.getText();
        if (username.isEmpty() && oldp.isEmpty() && newp.isEmpty()) JOptionPane.showMessageDialog(this,
                "Please enter credentials.", "Login error", JOptionPane.ERROR_MESSAGE);
        else if (username.contains("")) {
            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            System.out.println(userName);

            PasswordChangeRequest passwordChangeRequest = new PasswordChangeRequest(userName, hashedOldpassword, hashedNewPassword);
            oos.writeObject(passwordChangeRequest);
            oos.flush();
        }

        ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
        Object o = ois.readObject();
        if (o instanceof PasswordChangeReply) {
            PasswordChangeReply passWordChangeReply = (PasswordChangeReply) o;
            System.out.println("From here:" +passWordChangeReply.isSuccessful());
            if (passWordChangeReply.isSuccessful()) JOptionPane.showMessageDialog(this,
                    "Password Changed.", "successful", JOptionPane.PLAIN_MESSAGE);
            else JOptionPane.showMessageDialog(this, "Error, password not changed.",
                        "not successful", JOptionPane.ERROR_MESSAGE);
            socket.close();
            ois.close();
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object src = e.getSource();

        if (src == changeBtn) {
            try {
                //Hash passwordfield
                MessageDigest md = MessageDigest.getInstance("SHA-256");
                String userName = usernameText.getText();
                String oldPassword = oldPassText.getText();
                String newPassword = newPassText.getText();

                byte[] hashedOldPassword = md.digest(oldPassword.getBytes());
                byte[] hashedNewPassword = md.digest(newPassword.getBytes());

                String hashedOldpassword = byteToString(hashedOldPassword);
                String hashedNewpassword = byteToString(hashedNewPassword);
                ChangePassword(hashedOldpassword,hashedNewpassword,userName);

            } catch (NoSuchAlgorithmException | IOException | ClassNotFoundException noSuchAlgorithmException) {
                noSuchAlgorithmException.printStackTrace();
            }
        }
        else if (src == backBtn) {
            MainGui main = new MainGui();
            changeFrame.setVisible(false);
        }
    }
}


