package org.cab302.group52;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Base64;
import java.util.concurrent.Executors;
import java.util.concurrent.Flow;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Creates the window/full screen window that'll display the given billboard.
 */
public class SetupBillboard extends JFrame {

    Toolkit toolkit = Toolkit.getDefaultToolkit();
    Dimension screenSize = toolkit.getScreenSize();
    JFrame displayFrame;
    JPanel billboard = new JPanel();

    /**
     * Finds a matching RGB colour based on the given string.
     * @param txt The colour in string form.
     * @return The colour as a Color object.
     */
    private Color findRGB(String txt) {
        Color newColor = new Color(255, 255, 255);
        Pattern RPattern = Pattern.compile("(r=[0-9]+)");
        Pattern GPattern = Pattern.compile("(g=[0-9]+)");
        Pattern BPattern = Pattern.compile("(b=[0-9]+)");
        Matcher mR = RPattern.matcher(txt);
        Matcher mG = GPattern.matcher(txt);
        Matcher mB = BPattern.matcher(txt);
        if (mR.find() && mG.find() && mB.find()) {
            int intR = Integer.parseInt(mR.group(0).replaceAll("[^\\d]", ""));
            int intG = Integer.parseInt(mG.group(0).replaceAll("[^\\d]", ""));
            int intB = Integer.parseInt(mB.group(0).replaceAll("[^\\d]", ""));
            newColor = new Color(intR, intG, intB);
        }
        return newColor;
    }

    /**
     * Sets the background colour of the billboard to the given colour.
     * @param colour The colour to set.
     */
    private void backgroundSetup(String colour) {
        Color backColor = findRGB(colour);
        billboard.setBackground(backColor);
    }

    /**
     * Sets the billboard's message.
     * @param label The label to insert the message into.
     * @param message The message to display.
     * @param messageColour The colour of the message to display.
     * @param height The height of the message (for scaling purposes).
     */
    private void setMessage(JLabel label, String message, String messageColour, int height) {
        label.setText(message);

        Font labelFont = label.getFont();
        int stringWidth = label.getFontMetrics(labelFont).stringWidth(message);
        double widthRatio = (displayFrame.getWidth() - (displayFrame.getWidth() * 0.1)) / (double)stringWidth;
        int newFontSize = (int)(labelFont.getSize() * widthRatio);
        int fontSizeToUse = Math.min(newFontSize, height);
        label.setFont(new Font(StyleGuide.FONT, Font.BOLD, fontSizeToUse));

        label.setAlignmentX(Component.CENTER_ALIGNMENT);
        Color fontColor = findRGB(messageColour);
        label.setForeground(fontColor);
    }

    /**
     * Scales a buffered image based on the given label.
     * @param image The image to add to the label.
     * @param label The label to contain the image.
     * @return An unbuffered image with the correct dimensions.
     */
    private Image createImage(BufferedImage image, JLabel label) {
        Image scaled;
        if (image.getWidth() > image.getHeight() && image.getWidth() > label.getWidth()) {
            //wide image, bigger than label
            int scale = image.getWidth()/label.getWidth();
            int height = image.getHeight()/scale;
            scaled = image.getScaledInstance(label.getWidth(), height, Image.SCALE_SMOOTH);
        }
        else if (image.getWidth() > image.getHeight() && image.getWidth() < label.getWidth()) {
            //wide image, smaller than label
            int scale = label.getWidth()/image.getWidth();
            int height = image.getHeight()*scale;
            scaled = image.getScaledInstance(label.getWidth(), height, Image.SCALE_SMOOTH);
        }
        else if (image.getHeight() > image.getWidth() && image.getHeight() > label.getHeight()) {
            //tall image, bigger than label
            int scale = image.getHeight()/label.getHeight();
            int width = image.getWidth()/scale;
            scaled = image.getScaledInstance(width, label.getHeight(), Image.SCALE_SMOOTH);
        }
        else if (image.getHeight() > image.getWidth() && image.getHeight() < label.getHeight()) {
            //tall image, smaller than label
            int scale = label.getHeight()/image.getHeight();
            int width = image.getWidth()*scale;
            scaled = image.getScaledInstance(width, label.getHeight(), Image.SCALE_SMOOTH);
        }
        else scaled = image.getScaledInstance(label.getWidth(), label.getHeight(), Image.SCALE_SMOOTH);

        return scaled;
    }

    /**
     * Formats the data so that it can be processed correctly by the viewer.
     * @param picture The unformatted picture data.
     * @return The formatted picture data.
     */
    public static String findPictureData(String picture){
        String strPicture = "";
        Pattern RPattern = Pattern.compile("(?<=\\\").+");
        Matcher mR = RPattern.matcher(picture);
        if (mR.find()) strPicture = mR.group(0).replaceAll("[\"]", "");
        return strPicture;
    }

    /**
     * Adds the given picture to the given label.
     * @param label The label to contain the image.
     * @param picture The image to be added to the label.
     * @param size The necessary size of the label.
     */
    private void setImage(JLabel label, String picture, Dimension size){
        if (!picture.equals("")) {
            BufferedImage image;
            Icon icon;
            label.setSize(size);

            if (picture.contains("http")) {
                try {
                    image = null;
                    URL url = new URL(picture);
                    image = ImageIO.read(url);
                    Image scaled = createImage(image, label);
    
                    icon = new ImageIcon(scaled);
                    label.setIcon(icon);
    
                } catch (IOException e) { }
            } 
            else {
                try {
                    byte[] b = Base64.getMimeDecoder().decode(picture);
                    ByteArrayInputStream bis = new ByteArrayInputStream(b);
                    BufferedImage bImg = ImageIO.read(bis);

                    Image scaled = createImage(bImg, label);

                    icon = new ImageIcon(scaled);
                    label.setIcon(icon);
                }
                catch (IOException e) { }
            }
            label.setAlignmentX(Component.CENTER_ALIGNMENT);
        }
    }

    /**
     * Sets the billboard's information.
     * @param textPane The text pane to contain the information.
     * @param info The information to display.
     * @param infoColour The colour of the information to display.
     */
    private void setInfo(JTextPane textPane, String info, String infoColour) {
        SimpleAttributeSet attributeSet = new SimpleAttributeSet();
        StyleConstants.setFontFamily(attributeSet, StyleGuide.FONT);
        StyleConstants.setFontSize(attributeSet, displayFrame.getWidth() / 30);
        Color fontColour = findRGB(infoColour);
        StyleConstants.setForeground(attributeSet, fontColour);
        textPane.setCharacterAttributes(attributeSet, true);
        textPane.setText(info);
        textPane.setEditable(false);
    }

    /**
     * Sets up the frame to display the billboard. Will change size and window decoration based on whether it is a
     * preview or not.
     * @param preview Boolean to determine if it is a preview (true) or if it isn't (false).
     */
    private void setupFrame(boolean preview) {
        if (preview) {
            displayFrame = StyleGuide.setupMainFrame("Display Billboard", screenSize.width / 2,
                screenSize.height / 2,false,false);
        }
        else {
            displayFrame = StyleGuide.setupMainFrame("Display Billboard", screenSize.width, screenSize.height,
                    false,false);
            displayFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
            displayFrame.setUndecorated(true);
        }
    }

    /**
     * Adds the given message to the billboard.
     * @param message The message to display.
     * @param messageColour The colour of the message to display.
     * @param height The height of the message (for scaling purposes).
     */
    private void setupMessage(String message, String messageColour, int height) {
        JLabel messageLabel = new JLabel();
        setMessage(messageLabel, message, messageColour, height);
        messageLabel.setHorizontalAlignment(JLabel.CENTER);
        billboard.add(messageLabel);
    }

    /**
     * Adds the given picture to the billboard.
     * @param picture The picture to display.
     * @param size The size of the picture area.
     */
    private void setupPicture(String picture, Dimension size) {
        JLabel imageLabel = new JLabel();
        setImage(imageLabel, picture, size);
        imageLabel.setHorizontalAlignment(JLabel.CENTER);
        billboard.add(imageLabel);
    }

    /**
     * Adds the given information to the billboard.
     * @param info The information to display.
     * @param infoColour The colour of the information to display.
     * @param billboardColour The colour of the billboard (for formatting the text pane's background colour).
     */
    private void setupInfo(String info, String infoColour, String billboardColour) {
        JTextPane infoPane = new JTextPane();
        setInfo(infoPane, info, infoColour);
        infoPane.setBackground(findRGB(billboardColour));
        SimpleAttributeSet attribs = new SimpleAttributeSet();
        StyleConstants.setAlignment(attribs, StyleConstants.ALIGN_CENTER);
        infoPane.setParagraphAttributes(attribs, true);
        billboard.add(infoPane);
    }

    /**
     * Sets up the billboard to be displayed.
     * @param billboardColour The background colour of the billboard.
     * @param message The message to be displayed.
     * @param messageColour The colour of the message.
     * @param height The height of the message.
     */
    private void setupBillboard(String billboardColour, String message, String messageColour, int height) {
        backgroundSetup(billboardColour);
        billboard.setLayout(new GridLayout(1, 1));

        setupMessage(message, messageColour, height);
    }

    /**
     * Sets up the billboard to be displayed.
     * @param billboardColour The background colour of the billboard.
     * @param picture The picture to be displayed, in either URL or data form.
     */
    private void setupBillboard(String billboardColour, String picture, Dimension size) {
        backgroundSetup(billboardColour);
        billboard.setLayout(new GridLayout(1, 1));

        setupPicture(picture, size);
    }

    /**
     * Sets up the billboard to be displayed.
     * @param billboardColour The background colour of the billboard.
     * @param info The info to be displayed.
     * @param infoColour The colour of the info.
     */
    private void setupBillboard(String billboardColour, String info, String infoColour) {
        backgroundSetup(billboardColour);
        billboard.setLayout(new GridLayout(1, 1));

        setupInfo(info, infoColour, billboardColour);
    }

    /**
     * Sets up the billboard to be displayed.
     * @param billboardColour The background colour of the billboard.
     * @param message The message to be displayed.
     * @param messageColour The colour of the message.
     * @param height The height of the message.
     * @param info The info to be displayed.
     * @param infoColour The colour of the info.
     */
    private void setupBillboard(String billboardColour, String message, String messageColour, int height, String info,
                                String infoColour) {
        backgroundSetup(billboardColour);
        billboard.setLayout(new GridLayout(2, 1));

        setupMessage(message, messageColour, height);
        setupInfo(info, infoColour, billboardColour);
    }

    /**
     * Sets up the billboard to be displayed.
     * @param billboardColour The background colour of the billboard.
     * @param message The message to be displayed.
     * @param messageColour The colour of the message.
     * @param height The height of the message.
     * @param picture The picture to be displayed, in either URL or data form.
     */
    private void setupBillboard(String billboardColour, String message, String messageColour, int height,
                                String picture, Dimension size) {
        backgroundSetup(billboardColour);
        billboard.setLayout(new GridLayout(2, 1));

        setupMessage(message, messageColour, height);
        setupPicture(picture, size);
    }

    /**
     * Sets up the billboard to be displayed.
     * @param billboardColour The background colour of the billboard.
     * @param picture The picture to be displayed, in either URL or data form.
     * @param info The info to be displayed.
     * @param infoColour The colour of the info.
     */
    private void setupBillboard(String billboardColour, String picture, Dimension size, String info, String infoColour) {
        backgroundSetup(billboardColour);
        billboard.setLayout(new GridLayout(2, 1));

        setupPicture(picture, size);
        setupInfo(info, infoColour, billboardColour);
    }

    /**
     * Sets up the billboard to be displayed.
     * @param billboardColour The background colour of the billboard.
     * @param message The message to be displayed.
     * @param messageColour The colour of the message.
     * @param height The height of the message.
     * @param picture The picture to be displayed, in either URL or data form.
     * @param info The info to be displayed.
     * @param infoColour The colour of the info.
     */
    private void setupBillboard(String billboardColour, String message, String messageColour, int height,
                                String picture, Dimension size, String info, String infoColour) {
        backgroundSetup(billboardColour);
        billboard.setLayout(new GridLayout(3, 1));

        setupMessage(message, messageColour, height);
        setupPicture(picture, size);
        setupInfo(info, infoColour, billboardColour);
    }

    /**
     * Disposes of the current billboard frame.
     */
    private void destroyFrame() {
        displayFrame.setVisible(false);
        displayFrame.dispose();
    }

    /**
     * Starts a new window that displays a billboard based on the given parameters. Closes when ESC is pressed.
     * @param preview Whether or not this window is a preview window.
     * @param billboardColour The billboard's background colour.
     * @param message The message to be displayed.
     * @param messageColour The colour of the message to be displayed.
     * @param picture The picture to be displayed.
     * @param info The information to be displayed.
     * @param infoColour The colour of the information to be displayed.
     */
    public SetupBillboard(boolean preview, String billboardColour, String message, String messageColour, String picture,
                          String info, String infoColour) throws InterruptedException {
        super("Display Billboard");
        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();

        setupFrame(preview);
        int height = (int) (displayFrame.getHeight() / 1.1);
        int width = (int) (displayFrame.getWidth() / 1.1);

        if (!message.equals("") && picture.equals("") && info.equals(""))
            setupBillboard(billboardColour, message, messageColour, height);

        else if (message.equals("") && !picture.equals("") && info.equals(""))
            setupBillboard(billboardColour, picture, new Dimension(width, height));

        else if (message.equals("") && picture.equals("") && !info.equals(""))
            setupBillboard(billboardColour, info, infoColour);

        else if (!message.equals("") && !picture.equals("") && info.equals(""))
            setupBillboard(billboardColour, message, messageColour, height / 2, picture,
                    new Dimension(width / 2, height / 2));

        else if (!message.equals("") && picture.equals("") && !info.equals(""))
            setupBillboard(billboardColour, message, messageColour, displayFrame.getHeight() / 2, info,
                    infoColour);

        else if (message.equals("") && !picture.equals("") && !info.equals(""))
            setupBillboard(billboardColour, picture, new Dimension(width / 2, height / 2), info, infoColour);

        else if (!message.equals("") && !picture.equals("") && !info.equals(""))
            setupBillboard(billboardColour, message, messageColour, displayFrame.getHeight() / 3, picture,
                    new Dimension(width / 3, height / 3), info, infoColour);

        displayFrame.add(billboard);

        KeyStroke esc = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
        billboard.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(esc, "Exit");
        billboard.getActionMap().put("Exit", Exit);

        displayFrame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        displayFrame.setVisible(true);

        if (!preview) {
            Runnable task = () -> SwingUtilities.invokeLater(this::destroyFrame);
            executor.schedule(task, 15, TimeUnit.SECONDS);
        }
    }

    Action Exit = new AbstractAction() {
        public void actionPerformed(ActionEvent e) {
            displayFrame.setVisible(false);
        }
    };
}
