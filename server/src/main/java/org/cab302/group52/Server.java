package org.cab302.group52;

import org.cab302.group52.networkdata.Reply;
import org.cab302.group52.networkdata.billboard.*;
import org.cab302.group52.networkdata.schedule.*;
import org.cab302.group52.networkdata.user.*;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;

/**
 * Handles all the request data being sent to the server.
 */
public class Server {
    private static Connection database;
    private static Socket socket;
    
    /** 
     * The main server loop that handles the data being sent to the server
     * <p>
     * The server first establishes the database connection and sets up 
     * all the tables that are in use by the back end. After setting the 
     * server socket, it listens to any requests being sent by a client 
     * connected to the same port.
     * 
     * @param args
     * @throws SQLException
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws NoSuchAlgorithmException
     */
    public static void main(String[] args) throws ClassNotFoundException, IOException {
        setupDatabase();
        serverLoop();
    }

    public static void setupDatabase() {
        // Establishes the database tables and the default root user
        database = DBConnection.getDatabase();
        createUsersTable();
        createBillboardsTable();
        createScheduleTable();
    }

    public static void serverLoop() throws IOException, ClassNotFoundException {
        // Bind and listen to port 3129 for any client requests
        ServerSocket serverSocket = null;
        int serverPort = 3129;
        try {
            serverSocket = new ServerSocket(serverPort);
            System.out.println(String.format("Listening on port %d...", serverPort));
            for (;;) {
                socket = serverSocket.accept();
                System.out.println("Connected to" + socket.getInetAddress());
                ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
                Object object = inputStream.readObject();
                createRootUser("password");

                if (object instanceof BillboardAddRequest) {
                    BillboardAddRequest billboardAddRequest = (BillboardAddRequest) object;
                    addBillboard(billboardAddRequest);

                } else if (object instanceof BillboardRemoveRequest) {
                    BillboardRemoveRequest billboardRemoveRequest = (BillboardRemoveRequest) object;
                    removeBillboard(billboardRemoveRequest);

                } else if (object instanceof BillboardModifyRequest){
                    BillboardModifyRequest modifyRequest = (BillboardModifyRequest) object;
                    modifyBillboard(modifyRequest);

                } else if (object instanceof BillboardXMLRequest){
                    BillboardXMLRequest billboardXMLRequest = (BillboardXMLRequest) object;
                    getXml(billboardXMLRequest);

                } else if (object instanceof BillboardListRequest) {
                    BillboardListRequest billboardListRequest = (BillboardListRequest) object;
                    getBillboards(billboardListRequest);

                } else if (object instanceof ScheduleAddRequest) {
                    ScheduleAddRequest scheduleAddRequest = (ScheduleAddRequest) object;
                    addToSchedule(scheduleAddRequest);

                } else if (object instanceof ScheduleRemoveRequest) {
                    ScheduleRemoveRequest scheduleRemoveRequest = (ScheduleRemoveRequest) object;
                    removeFromSchedule(scheduleRemoveRequest);

                } else if (object instanceof ScheduleGetRequest) {
                    ScheduleGetRequest scheduleGetRequest = (ScheduleGetRequest) object;
                    getFromSchedule(scheduleGetRequest);
                
                } else if (object instanceof ScheduleListRequest) {
                    ScheduleListRequest scheduleListRequest = (ScheduleListRequest) object;
                    getAllSchedule(scheduleListRequest);

                } else if (object instanceof UserAddRequest) {
                    UserAddRequest createUserRequest = (UserAddRequest) object;
                    addUser(createUserRequest);

                } else if(object instanceof UserRemoveRequest){
                    UserRemoveRequest deleteUserRequest = (UserRemoveRequest) object;
                    removeUser(deleteUserRequest);

                } else if(object instanceof UserModifyRequest){
                    UserModifyRequest modifyUserRequest = (UserModifyRequest) object;
                    modifyUser(modifyUserRequest);

                } else if (object instanceof UserListRequest) {
                    UserListRequest userListRequest = (UserListRequest) object;
                    getUsers(userListRequest);

                } else if (object instanceof LoginRequest) {
                    LoginRequest loginRequest = (LoginRequest) object;
                    authenticateUser(loginRequest);

                } else if (object instanceof PasswordChangeRequest) {
                    PasswordChangeRequest passwordChangeRequest = (PasswordChangeRequest) object;
                    editPassword(passwordChangeRequest);
                }
            }
        } finally {
            serverSocket.close();
        }
    }

    /**
     * Initially sets up the users table
     */
    private static void createUsersTable() {
        String createTable = "CREATE TABLE IF NOT EXISTS users ("
            + "id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL UNIQUE,"
            + "username TEXT,"
            + "password TEXT,"
            + "salt TEXT,"
            + "create_billboard BOOLEAN,"
            + "edit_billboard BOOLEAN,"
            + "schedule_billboard BOOLEAN,"
            + "edit_users BOOLEAN" + ");";

        try{
            Statement statement = database.createStatement();
            statement.execute(createTable);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Initially sets up the billboards table
     */
    private static void createBillboardsTable() {
        String createTable = "CREATE TABLE IF NOT EXISTS billboards ("
            + "id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL UNIQUE,"
            + "user TEXT,"
            + "name TEXT,"
            + "xml TEXT" + ");";

        try{
            Statement statement = database.createStatement();
            statement.execute(createTable);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Initially sets up the schedule table
     */
    private static void createScheduleTable() {
        String createTable = "CREATE TABLE IF NOT EXISTS schedule ("
            + "id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL UNIQUE,"
            + "billboard_id INTEGER,"
            + "date DATE,"
            + "time INTEGER,"
            + "duration INTEGER,"
            + "recur_type INTEGER,"
            + "recur_interval INTEGER" + ");";

        try{
            Statement statement = database.createStatement();
            statement.execute(createTable);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /** 
     * Utilies a modify billboard request to update billboard database values.
     * 
     * @param request
     */
    private static void modifyBillboard(BillboardModifyRequest request) {
        String id = request.getId();
        String name = request.getName();
        String xml = request.getXML();
        String sqlRequest = "UPDATE billboards SET name=?, xml=? WHERE id=?";

        try {
            PreparedStatement statement = database.prepareStatement(sqlRequest);
            statement.setString(1, name);
            statement.setString(2, xml);
            statement.setString(3, id);
            int rowsUpdated = statement.executeUpdate();
            statement.close();

            if (rowsUpdated == 0) {
                System.out.println("Error updating billboard!");
                BillboardModifyReply reply = new BillboardModifyReply(false);
                sendReply(reply);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        System.out.println("Billboard Updated");
        BillboardModifyReply reply = new BillboardModifyReply(true);
        sendReply(reply);
    }
    
    /** 
     * Utilises a get XML request to send back billboard data from the database.
     * 
     * @param request
     */
    private static void getXml(BillboardXMLRequest request){
        String id = request.getId();
        ResultSet result = null;

        try {
            String sqlQuery = "SELECT * FROM billboards WHERE id=?";
            PreparedStatement statement = database.prepareStatement(sqlQuery);
            statement.setString(1, id);
            result = statement.executeQuery();
            
            while(result.next()) {
                String xml = result.getString("xml");
                BillboardXMLReply reply = new BillboardXMLReply(true, xml);
                sendReply(reply);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /** 
     * Utilises a billboard add request to add a new billboard to the database.
     * 
     * @param request
     */
    private static void addBillboard(BillboardAddRequest request) {
        String user = request.getUser();
        String name = request.getName();
        String xml = request.getXml();
        String sqlRequest = "INSERT INTO "
            + "billboards (user, name, xml) "
            + "VALUES (?,?,?)";

        try {
            PreparedStatement statement = database.prepareStatement(sqlRequest);
            statement.setString(1, user);
            statement.setString(2, name);
            statement.setString(3, xml);
            int rowsInserted = statement.executeUpdate();
            statement.close();

            if (rowsInserted == 0) {
                System.out.println("Error adding to database!");
                BillboardAddReply reply = new BillboardAddReply(false);
                sendReply(reply);
                return;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            return;
        }

        System.out.println("Added billboard " + request.getName() + " to database.");
        BillboardAddReply reply = new BillboardAddReply(true);
        sendReply(reply);
    }

    /** 
     * Utilises a remove billboard request to remove a billboard entry from the database.
     * 
     * @param request
     */
    private static void removeBillboard(BillboardRemoveRequest request) {
        String sqlRequest = "DELETE FROM billboards WHERE id=? ";

        try {
            PreparedStatement statement3 = database.prepareStatement(sqlRequest);
            statement3.setString(1, request.getId());
            int rowsDeleted = statement3.executeUpdate();
            statement3.close();

            if (rowsDeleted == 0) {
                System.out.println("That billboard does not exist!");
                BillboardRemoveReply reply = new BillboardRemoveReply(false);
                sendReply(reply);
                return;
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
            return;
        }

        System.out.println("A billboard with the ID " + request.getId() + " was deleted.");
        BillboardRemoveReply reply = new BillboardRemoveReply(true);
        sendReply(reply);
    }

    /** 
     * Utilises a list billboards request to send back a list of all billboards.
     * 
     * @param request
     */
    private static void getBillboards(BillboardListRequest request) {
        ArrayList<String> user = new ArrayList<String>();
        ArrayList<String> billboardId = new ArrayList<String>();
        ArrayList<String> billboardName = new ArrayList<String>();
        String sqlQuery = "SELECT * FROM billboards";

        try {
            PreparedStatement statement = database.prepareStatement(sqlQuery);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                billboardId.add(resultSet.getString("id"));
                user.add(resultSet.getString("user"));
                billboardName.add(resultSet.getString("name"));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            return;
        }

        BillboardListReply reply = new BillboardListReply(true, billboardId, user, billboardName);
        sendReply(reply);
    }

    /** 
     * Utilises a schedule add request to add a billboard to a schedule timeslot in the database.
     * 
     * @param request
     */
    private static void addToSchedule(ScheduleAddRequest request) {
        String sqlRequest = "INSERT INTO "
            + "schedule (billboard_id, date, time, duration, recur_type, recur_interval) "
            + "VALUES (?,?,?,?,?,?)";

        try {
            PreparedStatement statement = database.prepareStatement(sqlRequest);
            statement.setInt(1, request.getBillboardId());
            statement.setDate(2, request.getDate());
            statement.setInt(3, request.getTime());
            statement.setInt(4, request.getDuration());
            statement.setInt(5, request.getRecurType());
            statement.setInt(6, request.getRecurInterval());
            int rowsInserted = statement.executeUpdate();
            statement.close();

            if (rowsInserted == 0) {
                System.out.println("Error adding to database!");
                ScheduleAddReply reply = new ScheduleAddReply(false);
                sendReply(reply);
                return;
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
            return;
        }

        System.out.println("Added billboard " + request.getBillboardId() + " to schedule");
        ScheduleAddReply reply = new ScheduleAddReply(true);
        sendReply(reply);
    }

    /** 
     * Utilises a remove billboard request to remove a billboard entry from the database.
     * 
     * @param request
     */
    private static void removeFromSchedule(ScheduleRemoveRequest request) {
        String sqlRequest = "DELETE FROM schedule WHERE billboard_id=? AND date=? AND time=?";

        try {
            PreparedStatement statement3 = database.prepareStatement(sqlRequest);
            statement3.setInt(1, request.getBillboardId());
            statement3.setDate(2, request.getDate());
            statement3.setInt(3, request.getTime());
            int rowsDeleted = statement3.executeUpdate();
            statement3.close();

            if (rowsDeleted == 0) {
                System.out.println("That billboard does not exist!");
                BillboardRemoveReply reply = new BillboardRemoveReply(false);
                sendReply(reply);
                return;
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
            return;
        }

        System.out.println("Billboard " + request.getBillboardId() + " was removed from the schedule.");
        ScheduleRemoveReply reply = new ScheduleRemoveReply(true);
        sendReply(reply);
    }
    
    /** 
     * Utilises a schedule get request to send back the billboard data to be shown at the current time.
     * 
     * @param request
     */
    private static void getFromSchedule(ScheduleGetRequest request) {
        String sqlQuery = "SELECT * FROM schedule";
        ResultSet result = null;

        DateFormat date = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = date.format(new java.util.Date());
        Date currentDate = Date.valueOf(dateString);

        Integer hours = LocalDateTime.now().getHour();
        Integer minutes = LocalDateTime.now().getMinute();
        Integer currentMinute = (hours * 60) + minutes;

        Integer billboardId = 0;

        try {
            PreparedStatement statement = database.prepareStatement(sqlQuery);
            result = statement.executeQuery();
            statement.close();

            while (result.next()) {
                Integer billboardCheck = result.getInt("billboard_id");
                Date startDate = result.getDate("date");
                Integer startMinute = result.getInt("time");
                Integer endMinute = startMinute + result.getInt("duration");
                Integer recurType = result.getInt("recur_type");

                // Check for one off billboard in timeframe
                if (recurType == 1) {
                    System.out.println(currentDate.toString());
                    System.out.println(startDate.toString());
                    if (currentDate.compareTo(startDate) != 0) {
                        continue;
                    }

                    if (currentMinute > startMinute && currentMinute < endMinute) {
                        billboardId = billboardCheck;
                        break;
                    }
                }

                // Check for daily billboard in timeframe
                else if (recurType == 2) {
                    if (currentMinute > startMinute && currentMinute < endMinute) {
                        billboardId = billboardCheck;
                        break;
                    }
                }

                // Check for hourly billboard in timeframe
                else if (recurType == 3) {
                    Integer addedStartMinute = startMinute;
                    Integer addedEndMinute = endMinute;
                    while (endMinute < 2400) {
                        if (currentMinute > addedStartMinute && currentMinute < addedEndMinute) {
                            billboardId = billboardCheck;
                            break;
                        }
                        addedStartMinute = addedStartMinute + 60;
                        addedEndMinute =  addedEndMinute + 60;
                    }
                    
                    Integer subtractedStartMinute = startMinute;
                    Integer subtractedEndMinute = endMinute;
                    while(startMinute > 0) {
                        if (currentMinute > subtractedStartMinute && currentMinute < subtractedEndMinute) {
                            billboardId = billboardCheck;
                            break;
                        }
                        subtractedStartMinute = subtractedStartMinute - 60;
                        subtractedEndMinute =  subtractedEndMinute - 60;
                    }
                }

                // Check for x interval of minutes of billboard in timeframe
                else if (recurType == 4) {
                    Integer recurInterval = result.getInt(7);
                    Integer addedStartMinute = startMinute;
                    Integer addedEndMinute = endMinute;
                    while (endMinute < 2400) {
                        if (currentMinute > addedStartMinute && currentMinute < addedEndMinute) {
                            billboardId = billboardCheck;
                            break;
                        }
                        addedStartMinute = addedStartMinute + recurInterval;
                        addedEndMinute =  addedEndMinute + recurInterval;
                    }
                    
                    Integer subtractedStartMinute = startMinute;
                    Integer subtractedEndMinute = endMinute;
                    while(startMinute > 0) {
                        if (currentMinute > subtractedStartMinute && currentMinute < subtractedEndMinute) {
                            billboardId = billboardCheck;
                            break;
                        }
                        subtractedStartMinute = subtractedStartMinute - recurInterval;
                        subtractedEndMinute =  subtractedEndMinute - recurInterval;
                    }
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            return;
        }

        if (billboardId != 0) {
            String sqlQuery2 = "SELECT * FROM billboards WHERE id=?";
            try {
                PreparedStatement statement = database.prepareStatement(sqlQuery2);
                statement.setInt(1, billboardId);
                result = statement.executeQuery();
                statement.close();
                while (result.next()) {
                    ScheduleGetReply reply = new ScheduleGetReply(true, result.getString("xml"));
                    sendReply(reply);
                    return;
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
                return;
            }
        }

        ScheduleGetReply reply = new ScheduleGetReply(false, "");
        sendReply(reply);
    }

    /**
     * Utilises a list schedule request to send back a list of all billboard schedules.
     * 
     * @param request
     */
    private static void getAllSchedule(ScheduleListRequest request) {
        ArrayList<Integer> id = new ArrayList<Integer>();
        ArrayList<Integer> billboardId = new ArrayList<Integer>();
        ArrayList<Date> date = new ArrayList<Date>();
        ArrayList<Integer> time = new ArrayList<Integer>();
        ArrayList<Integer> duration = new ArrayList<Integer>();
        ArrayList<Integer> recurType = new ArrayList<Integer>();
        ArrayList<Integer> recurInterval = new ArrayList<Integer>();
        String sqlQuery = "SELECT * FROM schedule";

        try {
            PreparedStatement statement = database.prepareStatement(sqlQuery);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                id.add(resultSet.getInt("id"));
                billboardId.add(resultSet.getInt("billboard_id"));
                date.add(resultSet.getDate("date"));
                time.add(resultSet.getInt("time"));
                duration.add(resultSet.getInt("duration"));
                recurType.add(resultSet.getInt("recur_type"));
                recurInterval.add(resultSet.getInt("recur_interval"));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            return;
        }

        ScheduleListReply reply = new ScheduleListReply(true, id, billboardId, date, time, duration, recurType,
            recurInterval);
        sendReply(reply);
    }
    
    /** 
     * Utilises an add user request to add a new user to the database.
     * 
     * @param request
     */
    private static void addUser(UserAddRequest request) {
        String receivedHashedPassword = request.getPassword();
        String salt = generateSalt();
        String saltedpassword = (receivedHashedPassword + salt);
        String hashedPassword = generateHash(saltedpassword);
        String sqlRequest = "INSERT INTO "
            + "users (username, password, salt, create_billboard, edit_billboard, schedule_billboard, edit_users) "
            + "VALUES (?,?,?,?,?,?,?)";

        try {
            PreparedStatement statement = database.prepareStatement(sqlRequest);
            statement.setString(1, request.getUsername());
            statement.setString(2, hashedPassword);
            statement.setString(3, salt);
            statement.setBoolean(4, request.getCreateBillboard());
            statement.setBoolean(5, request.editBillboard());
            statement.setBoolean(6, request.scheduleBillboard());
            statement.setBoolean(7, request.editUser());
            int rowsInserted = statement.executeUpdate();
            statement.close();

            if (rowsInserted == 0) {
                System.out.println("Error adding to database!");
                UserAddReply reply = new UserAddReply(false);
                sendReply(reply);
                return;
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        System.out.println("Added user " + request.getUsername() + " to database");
        UserAddReply reply = new UserAddReply(true);
        sendReply(reply);
    }

    /** 
     * Utilises a remove user request to remove a user from the database.
     * 
     * @param request
     */
    private static void removeUser(UserRemoveRequest request) {
        String sqlRequest = "DELETE FROM users WHERE id = ?";

        try {
            PreparedStatement statement = database.prepareStatement(sqlRequest);
            statement.setString(1, request.getUserId());
            int rowsDeleted = statement.executeUpdate();
            statement.close();

            if (rowsDeleted == 0) {
                System.out.println("That user does not exist!");
                UserRemoveReply reply = new UserRemoveReply(false);
                sendReply(reply);
                return;
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        System.out.println("A user with the ID " + request.getUserId() + " was deleted");
        UserRemoveReply reply = new UserRemoveReply(true);
        sendReply(reply);
    }

    /** 
     * Utilies a modify billboard request to update billboard database values.
     * 
     * @param request
     */
    private static void modifyUser(UserModifyRequest request) {
        String sqlRequest = "UPDATE billboards SET create_billboard=?, edit_billboard=?,"
            + "schedule_billboard=?, edit_users=? WHERE id=?";

        try {
            PreparedStatement statement = database.prepareStatement(sqlRequest);
            statement.setString(1, request.getCreateBillboard());
            statement.setString(2, request.getEditBillboard());
            statement.setString(3, request.getScheduleBillboard());
            statement.setString(4, request.getEditUsers());
            statement.setString(5, request.getId());
            int rowsUpdated = statement.executeUpdate();
            statement.close();

            if (rowsUpdated == 0) {
                System.out.println("Error updating billboard!");
                BillboardModifyReply reply = new BillboardModifyReply(false);
                sendReply(reply);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        System.out.println("User with id " + request.getId() + " permissions updated!");
        BillboardModifyReply reply = new BillboardModifyReply(true);
        sendReply(reply);
    }
    
    /** 
     * Utilises a get users request to send back a list of all users from the database.
     * 
     * @param request
     */
    private static void getUsers(UserListRequest request) {
        boolean load = request.loadUsers();
        ArrayList<String> userId = new ArrayList<>();
        ArrayList<String> username = new ArrayList<>();
        ArrayList<String> password = new ArrayList<>();
        ArrayList<String> createbillboard = new ArrayList<>();
        ArrayList<String> editbillboard = new ArrayList<>();
        ArrayList<String> schedulebillboard = new ArrayList<>();
        ArrayList<String> editusers = new ArrayList<>();


        if (!load) {
            return;
        }

        try {
            String sqlQuery = "SELECT * FROM users";
            PreparedStatement statement = database.prepareStatement(sqlQuery);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                userId.add(resultSet.getString(1));
                username.add(resultSet.getString(2));
                password.add(resultSet.getString(3));
                createbillboard.add(resultSet.getString(5));
                editbillboard.add(resultSet.getString(6));
                schedulebillboard.add(resultSet.getString(7));
                editusers.add(resultSet.getString(8));

            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        UserListReply reply = new UserListReply(true,userId, username, password, createbillboard, editbillboard,
            schedulebillboard, editusers);
        sendReply(reply);
    }
  
    /** 
     * Utilises an authentication request to confirm a user's login details 
     * and send back the session details to the client.
     * 
     * @param request
     */
    private static void authenticateUser(LoginRequest request) {
        ResultSet result = null;

        try {
            // Check to see if username is a valid database entry
            String sqlQuery = "SELECT * FROM users where username=?";
            PreparedStatement statement = database.prepareStatement(sqlQuery);
            statement.setString(1, request.getUsernameValidation());
            result = statement.executeQuery();
            if (result.next() == false) {
                System.out.println("User credentials incorrect");
                LoginReply reply = new LoginReply(false);
                sendReply(reply);
                return;
            }

            // Check to see if password is valid by combining the received
            // hashed password with the salt, and then comparing the rehashed
            // password with the database entry
            String password = result.getString("password");
            String salt = result.getString("salt");
            String saltedpass = request.getPasswordValidation() + salt;
            String hashedPassword = generateHash(saltedpass);
            if (!hashedPassword.equals(password)) {
                System.out.println("User credentials incorrect");
                LoginReply reply = new LoginReply(false);
                sendReply(reply);
                return;
            }

            // Add user permissions to the login reply
            String user = result.getString("username");
            ArrayList<String> newCreatePermission = new ArrayList<>();
            newCreatePermission.add(result.getString("create_billboard"));
            String createPermission = result.getString("create_billboard");
            String editBillboardPermission = result.getString("edit_billboard");
            String schedulePermission = result.getString("schedule_billboard");
            String editUsersPermission = result.getString("edit_users");
            String sessionToken = generateSalt();
            LoginReply reply = new LoginReply(true, sessionToken, user, newCreatePermission, 
                createPermission, editBillboardPermission, schedulePermission, editUsersPermission);
            sendReply(reply);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return;
    }
    
    /** 
     * Utilises an edit password request to edit a user's password in the database.
     * 
     * @param request
     */
    private static void editPassword(PasswordChangeRequest request) {
        String sqlQuery = "SELECT * FROM users where username=?";

        try {
            PreparedStatement uStatement = database.prepareStatement(sqlQuery);
            uStatement.setString(1, request.getUserName());
            System.out.println(request.getUserName());
            ResultSet result = uStatement.executeQuery();

            // Cancel if user doesn't exist
            while (result.next()) {
                // Cancel if old password is the same as the new password
                String newPassword = request.getNewPassword();
                String salt = result.getString("salt");
                String newSaltedPassword = newPassword + salt;
                String newHashedPassword = generateHash(newSaltedPassword);
                String oldHashedPassword = result.getString("password");
                if (newHashedPassword == oldHashedPassword) {
                    PasswordChangeReply reply = new PasswordChangeReply(false);
                    sendReply(reply);
                    return;
                }

                // Update password entry for specified user
                String sqlRequest = "UPDATE users SET password=? ,salt=? WHERE username=? ";
                PreparedStatement pStatement = database.prepareStatement(sqlRequest);
                pStatement.setString(1, newHashedPassword);
                pStatement.setString(2, salt);
                pStatement.setString(3, request.getUserName());
                int rowsUpdated = pStatement.executeUpdate();
                if (rowsUpdated == 0) {
                    PasswordChangeReply reply = new PasswordChangeReply(false);
                    sendReply(reply);
                    return;
                }

                System.out.println("An existing user was updated successfully!");
                PasswordChangeReply reply = new PasswordChangeReply(true);
                sendReply(reply);
                return;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        PasswordChangeReply reply = new PasswordChangeReply(false);
        sendReply(reply);
    }
    
    /** 
     * Creates the initial root user with access to all permissions.
     * 
     * @param password
     */
    private static void createRootUser(String password) {
        // Cancel if root user already exists
        String sqlQuery = "SELECT * FROM users WHERE username = 'root'";
        try{
            Statement statement = database.createStatement();
            ResultSet result = statement.executeQuery(sqlQuery);
            if (result.next() == true)
            {
                return;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        // Add root user to database
        String hashedPassword = generateHash(password);
        UserAddRequest user = new UserAddRequest("root", hashedPassword, true, true, true, true);
        addUser(user);
    }

    /** 
     * Generates a 32 byte salt to be utilised as a password randomiser.
     * 
     * @return String
     */
    private static String generateSalt() {
        SecureRandom rng = new SecureRandom();
        byte[] saltBytes = new byte[32];
        rng.nextBytes(saltBytes);
        String saltString = byteToString(saltBytes);
        return saltString;
    }

    /** 
     * Takes a password and hashes it using the SHA-256 algorithm.
     * 
     * @param unhashed
     * @return String
     */
    private static String generateHash(String unhashed)
    {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] hashedBytes = md.digest(unhashed.getBytes());
            String hashed = byteToString(hashedBytes);
            return hashed;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    /** 
     * Translates a string of bytes into a string of chars.
     * 
     * @param hash
     * @return String
     */
    private static String byteToString(byte[] hash) {
        StringBuffer sb = new StringBuffer();
        for (byte b : hash) {
            sb.append(String.format("%02x", b & 0xFF));
        }
        return sb.toString();
    }
    
    /** 
     * Used to send a reply object back to the client.
     * 
     * @param reply
     */
    private static void sendReply(Reply reply) {
        try {
            ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
            outputStream.writeObject(reply);
            outputStream.flush();
        } catch (IOException ex) {
            System.out.println("Reply not recieved.");
        }
    }
}