package org.cab302.group52;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Handles the SQL database connections.
 */
public class DBConnection {
    private static Connection database = null;

    /**
     * Constructs a database connection with the db.props configuration.
     */
    private DBConnection() {
        Properties config = new Properties();
        InputStream file = null;

        try {
            // Attempt to get the db.props config file
            System.out.println("Fetching configuration...");
            file = DBConnection.class.getClassLoader()
                .getResourceAsStream("db.props");
            config.load(file);
            file.close();
  
            // Fetch each variable from the config
            String url = config.getProperty("server.url");
            String schema = config.getProperty("server.schema");
            String username = config.getProperty("server.username");
            String password = config.getProperty("server.password");
  
            // Connect to the database
            System.out.println(String.format("Attempting to connect to database '%s'...", schema));
            database = DriverManager.getConnection(url + "/" + schema,
                username, password);
            System.out.println("Database connection successful!");

        } catch (SQLException sqle) {
            System.err.println(sqle);
        } catch (FileNotFoundException fnfe) {
            System.err.println(fnfe);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /** 
     * Fetches the database connection or establishes it if no connection has been made yet.
     * 
     * @return Connection
     */
    public static Connection getDatabase() {
        if (database == null) {
           new DBConnection();
        }
        return database;
    }
}