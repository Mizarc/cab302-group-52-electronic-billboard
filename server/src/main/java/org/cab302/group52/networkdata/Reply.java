package org.cab302.group52.networkdata;

import java.io.Serializable;

/**
 * A generic reply that tells the client whether the action was successful or not.
 */
public abstract class Reply implements Serializable {
    private static final long serialVersionUID = 1L;
    private boolean successful;

    /**
     * Constructs a reply based on the status of the request
     * 
     * @param successful
     */
    protected Reply(boolean successful) {
        this.successful = successful;
    }

    public boolean isSuccessful() { return successful; }
}