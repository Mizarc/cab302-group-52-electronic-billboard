package org.cab302.group52.networkdata.user;

import java.io.Serializable;

/**
 * Sent by the client to request a list of all users.
 */
public class UserListRequest implements Serializable {
    private static final long serialVersionUID = 1L;
    private boolean loadUsers;

    public UserListRequest(boolean loadUsers) {
        this.loadUsers = loadUsers;


    }

    public boolean loadUsers() { return loadUsers; }
}