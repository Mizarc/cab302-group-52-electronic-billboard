package org.cab302.group52.networkdata.user;

import java.util.ArrayList;

import org.cab302.group52.networkdata.Reply;

/**
 * Sends the session details of the user back to the client.
 */
public class LoginReply extends Reply {
    private static final long serialVersionUID = 1L;
    private ArrayList<String> newCreatePermission;
    private String sessionToken;
    private String user;
    private String createPermission;
    private String editBillboardPermission;
    private String schedulePermission;
    private String editUsersPermission;

    public LoginReply(boolean successful, String sessionToken, String user, ArrayList<String> newCreatePermission, 
            String createPermission, String editBillboardPermission, String schedulePermission,
            String editUsersPermission) {
        super(successful);
        this.sessionToken = sessionToken;
        this.user = user;
        this.newCreatePermission = newCreatePermission;
        this.createPermission = createPermission;
        this.editBillboardPermission = editBillboardPermission;
        this.schedulePermission = schedulePermission;
        this.editUsersPermission = editUsersPermission;
    }

    public LoginReply(boolean successful) {
        super(successful);
    }

    public ArrayList<String> getNewCreatePermission() { return newCreatePermission; }
    public String getSessionToken() { return sessionToken; }
    public String getUser() { return user; }
    public String getCreatePermission() { return createPermission; }
    public String getEditBillboardPermission() { return editBillboardPermission; }
    public String getSchedulePermission() { return schedulePermission; }
    public String getEditUsersPermission() { return editUsersPermission; }
}
