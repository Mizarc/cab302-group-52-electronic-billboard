package org.cab302.group52.networkdata.schedule;

import org.cab302.group52.networkdata.Reply;

/**
 * Sends the current billboard data that has to be displayed from the schedule back to the client.
 */
public class ScheduleGetReply extends Reply {
    private static final long serialVersionUID = 1L;
    private final String XML;
    
    public ScheduleGetReply(boolean successful, String XML) {
        super(successful);
        this.XML = XML;
    }

    public String getXML(){ return XML; }
}