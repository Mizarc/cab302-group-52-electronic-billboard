package org.cab302.group52.networkdata.billboard;

import org.cab302.group52.networkdata.Reply;

/**
 * Sends a billboard removal success report back to the client.
 */
public class BillboardRemoveReply extends Reply {
    private static final long serialVersionUID = 1L;

    public BillboardRemoveReply(boolean successful) {
        super(successful);
    }
}