package org.cab302.group52.networkdata.schedule;

import java.sql.Date;
import java.util.ArrayList;

import org.cab302.group52.networkdata.Reply;

/**
 * Sends a list of all billboards back to the client.
 */
public class ScheduleListReply extends Reply {
    private static final long serialVersionUID = 1L;
    ArrayList<Integer> id;
    ArrayList<Integer> billboardId;
    ArrayList<Date> date;
    ArrayList<Integer> time;
    ArrayList<Integer> duration;
    ArrayList<Integer> recurType;
    ArrayList<Integer> recurInterval;

    public ScheduleListReply (boolean successful, ArrayList<Integer> id, ArrayList<Integer> billboardId, 
            ArrayList<Date> date, ArrayList<Integer> time, ArrayList<Integer> duration, ArrayList<Integer> recurType,
            ArrayList<Integer> recurInterval) {
        super(successful);
        this.id = id;
        this.billboardId = billboardId;
        this.date = date;
        this.time = time;
        this.duration = duration;
        this.recurType = recurType;
        this.recurInterval = recurInterval;
    }

    public ArrayList<Integer> getId() { return id; }
    public ArrayList<Integer> getBillboardId() { return billboardId; }
    public ArrayList<Date> getDate() { return date; }
    public ArrayList<Integer> getTime() { return time; }
    public ArrayList<Integer> getDuration() { return duration; }
    public ArrayList<Integer> getRecurType() { return recurType; }
    public ArrayList<Integer> getRecurInterval() { return recurInterval; }
}
