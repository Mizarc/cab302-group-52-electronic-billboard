package org.cab302.group52.networkdata.user;

import org.cab302.group52.networkdata.Reply;

/**
 * Sends a user modification success report back to the client.
 */
public class UserModifyReply extends Reply {
    private static final long serialVersionUID = 1L;

    public UserModifyReply(boolean successful) {
        super(successful);
    }
}