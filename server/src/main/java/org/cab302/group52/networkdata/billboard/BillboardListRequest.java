package org.cab302.group52.networkdata.billboard;

import java.io.Serializable;

/**
 * Sent by the client to request a list of all billboards
 */
public class BillboardListRequest implements Serializable {
    private static final long serialVersionUID = 1L;

    public BillboardListRequest() {}
}
