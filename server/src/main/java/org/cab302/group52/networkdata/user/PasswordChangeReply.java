package org.cab302.group52.networkdata.user;

import org.cab302.group52.networkdata.Reply;

/**
 * Sends a password change success report back to the client.
 */
public class PasswordChangeReply extends Reply {
    private static final long serialVersionUID = 1L;

    public PasswordChangeReply(boolean successful) {
        super(successful);
    }
}