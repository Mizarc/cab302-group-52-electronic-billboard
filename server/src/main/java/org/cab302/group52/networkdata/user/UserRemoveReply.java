package org.cab302.group52.networkdata.user;

import org.cab302.group52.networkdata.Reply;

/**
 * Sends a user removal success report back to the client
 */
public class UserRemoveReply extends Reply {
    private static final long serialVersionUID = 1L;

    public UserRemoveReply(boolean successful) {
        super(successful);
    }
}