package org.cab302.group52.networkdata.schedule;

import java.io.Serializable;

/**
 * Sent by the client to request the server for the current billboard data in the schedule timeframe.
 */
public class ScheduleGetRequest implements Serializable {
    private static final long serialVersionUID = 1L;

    public ScheduleGetRequest() {}
}