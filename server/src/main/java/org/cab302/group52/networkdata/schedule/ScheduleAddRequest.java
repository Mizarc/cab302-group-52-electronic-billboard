package org.cab302.group52.networkdata.schedule;

import java.io.Serializable;
import java.sql.Date;

/**
 * Sent by the client to add a billboard to a specified timeframe in the schedule.
 */
public class ScheduleAddRequest implements Serializable {
    private static final long serialVersionUID = 1L;
    private final Integer billboardId;
    private final Date date;
    private final Integer time;
    private final Integer duration;
    private final Integer recurType;
    private final Integer recurInterval;

    public ScheduleAddRequest (Integer billboardId, Date date, Integer time, Integer duration, Integer recurType,
            Integer recurInterval) {
        this.billboardId = billboardId;
        this.date = date;
        this.time = time;
        this.duration = duration;
        this.recurType = recurType;
        this.recurInterval = recurInterval;
    }

    public Integer getBillboardId() { return billboardId; }
    public Date getDate() { return date; }
    public Integer getTime() { return time; }
    public Integer getDuration() { return duration; }
    public Integer getRecurType() { return recurType; }
    public Integer getRecurInterval() { return recurInterval; }
}