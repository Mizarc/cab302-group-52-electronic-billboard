package org.cab302.group52.networkdata.user;

import org.cab302.group52.networkdata.Reply;

/**
 * Sends a user creation success report back to the client
 */
public class UserAddReply extends Reply {
    private static final long serialVersionUID = 1L;

    public UserAddReply(boolean successful) {
        super(successful);
    }
}