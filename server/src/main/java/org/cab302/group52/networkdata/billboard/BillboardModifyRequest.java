package org.cab302.group52.networkdata.billboard;

import java.io.Serializable;

/**
 * Sent by the client to implement changes to a specified billboard;
 */
public class BillboardModifyRequest implements Serializable {
    private static final long serialVersionUID = 1L;
    private final String id;
    private final String name;
    private final String XML;

    public BillboardModifyRequest (String id, String name, String XML) {
        this.id = id;
        this.name = name;
        this.XML = XML;
    }

    public String getId() { return id; }
    public String getName() { return name; }
    public String getXML() { return XML; }
}