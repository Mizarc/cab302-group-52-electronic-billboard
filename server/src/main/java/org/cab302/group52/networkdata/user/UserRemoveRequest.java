package org.cab302.group52.networkdata.user;

import java.io.Serializable;

/**
 * Sent by the client to request the removal of a user.
 */
public class UserRemoveRequest implements Serializable {
    private static final long serialVersionUID = 1L;
    private final String userId;

    public UserRemoveRequest (String userId) {
        this.userId = userId;
    }

    public String getUserId() {  return userId; }
}
