package org.cab302.group52.networkdata.billboard;

import org.cab302.group52.networkdata.Reply;

/**
 * Sends a billboard modification success report back to the client.
 */
public class BillboardModifyReply extends Reply {
    private static final long serialVersionUID = 1L;

    public BillboardModifyReply(boolean successful) {
        super(successful);
    }
}