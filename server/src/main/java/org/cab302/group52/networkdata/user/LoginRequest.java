package org.cab302.group52.networkdata.user;

import java.io.Serializable;

/**
 * Sent by the client to request session details of the login details.
 */
public class LoginRequest implements Serializable {
    private static final long serialVersionUID = 1L;
    private final String username;
    private final String password;

    public LoginRequest(String username, String password){
        this.username = username;
        this.password = password;
    }

    public String getUsernameValidation() { return username; }
    public String getPasswordValidation() { return password; }
}
