package org.cab302.group52.networkdata.billboard;

import java.io.Serializable;

/**
 * Sent by the client to request XML data of a specified billboard.
 */
public class BillboardXMLRequest implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id;

    public BillboardXMLRequest(String id) {
        this.id = id;
    }

    public String getId() { return id; }
}
