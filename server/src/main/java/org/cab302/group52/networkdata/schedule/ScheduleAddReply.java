package org.cab302.group52.networkdata.schedule;

import org.cab302.group52.networkdata.Reply;

/**
 * Sends a schedule addition success report back to the client
 */
public class ScheduleAddReply extends Reply {
    private static final long serialVersionUID = 1L;

    public ScheduleAddReply(boolean successful) {
        super(successful);
    }
}