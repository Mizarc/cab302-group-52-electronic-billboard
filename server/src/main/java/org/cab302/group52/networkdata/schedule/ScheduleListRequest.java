package org.cab302.group52.networkdata.schedule;

import java.io.Serializable;

/**
 * Sent by the client to request a list of all billboards
 */
public class ScheduleListRequest implements Serializable {
    private static final long serialVersionUID = 1L;

    public ScheduleListRequest() {}
}
