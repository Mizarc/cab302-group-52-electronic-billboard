package org.cab302.group52.networkdata.billboard;

import java.io.Serializable;

/**
 * Sent by the client to request the server to add a new billboard with specified data.
 */
public class BillboardAddRequest implements Serializable {
    private static final long serialVersionUID = 1L;
    private final String user;
    private final String name;
    private final String XML;

    public BillboardAddRequest(String user, String name, String XML) {
        this.user = user;
        this.name = name;
        this.XML = XML;
    }

    public String getXml() {return XML;}
    public String getUser() { return user; }
    public String getName() { return name; }
}
