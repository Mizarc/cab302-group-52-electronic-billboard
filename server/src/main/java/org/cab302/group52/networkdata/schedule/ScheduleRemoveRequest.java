package org.cab302.group52.networkdata.schedule;

import java.io.Serializable;
import java.sql.Date;

/**
 * Sent by the client to request the removal of a specified billboard.
 */
public class ScheduleRemoveRequest implements Serializable {
    private static final long serialVersionUID = 1L;
    private final Integer billboardId;
    private final Date date;
    private final Integer time;

    public ScheduleRemoveRequest(Integer billboardId, Date date, Integer time){
        this.billboardId = billboardId;
        this.date = date;
        this.time = time;
    }

    public Integer getBillboardId() { return billboardId; }
    public Date getDate() { return date; }
    public Integer getTime() { return time; }
}
