package org.cab302.group52.networkdata.billboard;

import org.cab302.group52.networkdata.Reply;

/**
 * Sends a billboard creation success report back to the client
 */
public class BillboardAddReply extends Reply {
    private static final long serialVersionUID = 1L;

    public BillboardAddReply(boolean successful) {
        super(successful);
    }
}
