package org.cab302.group52.networkdata.user;

import java.io.Serializable;

/**
 * Sent by the client to request a password change of a specified user.
 */
public class PasswordChangeRequest implements Serializable {
    private static final long serialVersionUID = 1L;
    private String username;
    private String oldPassword;
    private String newPassword;

    public PasswordChangeRequest (String username, String oldPassword, String newPassword) {
        this.username = username;
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
    }

    public String getUserName() { return username; }
    public String getOldPassword() { return oldPassword; }
    public String getNewPassword() { return newPassword; }
}