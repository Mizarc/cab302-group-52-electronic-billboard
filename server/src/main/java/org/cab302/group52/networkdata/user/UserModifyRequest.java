package org.cab302.group52.networkdata.user;

import java.io.Serializable;

/**
 * Sent by the client to implement changes to a specified billboard;
 */
public class UserModifyRequest implements Serializable {
    private static final long serialVersionUID = 1L;
    private final String id;
    private final String createBillboard;
    private final String editBillboard;
    private final String scheduleBillboard;
    private final String editUsers;

    public UserModifyRequest (String id, String createBillboard, String editBillboard, String scheduleBillboard,
    String editUsers) {
        this.id = id;
        this.createBillboard = createBillboard;
        this.editBillboard = editBillboard;
        this.scheduleBillboard = scheduleBillboard;
        this.editUsers = editUsers;
    }

    public String getId() { return id; }
    public String getCreateBillboard() { return createBillboard; }
    public String getEditBillboard() { return editBillboard; }
    public String getScheduleBillboard() { return scheduleBillboard; }
    public String getEditUsers() { return editUsers; }
}