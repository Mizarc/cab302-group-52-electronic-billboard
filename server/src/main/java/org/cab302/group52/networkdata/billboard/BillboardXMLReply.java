package org.cab302.group52.networkdata.billboard;

import org.cab302.group52.networkdata.Reply;

/**
 * Sends the XML data of a specified billboard back to the client.
 */
public class BillboardXMLReply extends Reply {
    private static final long serialVersionUID = 1L;
    private final String XML;


    public BillboardXMLReply(boolean successful, String XML) {
        super(successful);
        this.XML = XML;
    }

    public String getXML() { return XML; }
}
