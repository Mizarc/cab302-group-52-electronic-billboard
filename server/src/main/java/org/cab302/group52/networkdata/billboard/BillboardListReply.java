package org.cab302.group52.networkdata.billboard;

import java.util.ArrayList;

import org.cab302.group52.networkdata.Reply;

/**
 * Sends a list of all billboards back to the client.
 */
public class BillboardListReply extends Reply {
    private static final long serialVersionUID = 1L;
    private ArrayList<String> billboardId;
    private ArrayList<String> user;
    private ArrayList<String> billboardName;

    public BillboardListReply (boolean successful, ArrayList<String> billboardId, ArrayList<String> user, ArrayList<String> billboardName) {
        super(successful);
        this.user = user;
        this.billboardId = billboardId;
        this.billboardName = billboardName;
    }

    public ArrayList<String> getUser() { return user; }
    public ArrayList<String> getBillboardId() { return billboardId; }
    public ArrayList<String> getBillboardName() { return billboardName; }
}
