package org.cab302.group52.networkdata.schedule;

import org.cab302.group52.networkdata.Reply;

/**
 * Sends a schedule removal success report back to the client
 */
public class ScheduleRemoveReply extends Reply {
    private static final long serialVersionUID = 1L;

    public ScheduleRemoveReply(boolean successful) {
        super(successful);
    }
}