package org.cab302.group52.networkdata.user;

import java.io.Serializable;

/**
 * Sent by the client to request the creation of a new user.
 */
public class UserAddRequest implements Serializable {
    private static final long serialVersionUID = 1L;
    private final String username;
    private final String password;
    private final boolean createBillboard;
    private final boolean editBillboard;
    private final boolean scheduleBillboard;
    private final boolean editUser;

    public UserAddRequest (String username, String password, boolean createBillboard, boolean editBillboard,
            boolean scheduleBillboard, boolean editUser){
        this.username = username;
        this.password = password;
        this.createBillboard = createBillboard;
        this.editBillboard = editBillboard;
        this.scheduleBillboard = scheduleBillboard;
        this.editUser = editUser;
    }

    public String getUsername() { return username; }
    public String getPassword() { return password; }
    public boolean getCreateBillboard() { return createBillboard; }
    public boolean editBillboard() { return editBillboard; }
    public boolean scheduleBillboard() { return scheduleBillboard; }
    public boolean editUser() { return editUser; }
}