package org.cab302.group52.networkdata.user;

import java.util.ArrayList;

import org.cab302.group52.networkdata.Reply;

/**
 * Sends a list of all users back to the client.
 */
public class UserListReply extends Reply {
    private static final long serialVersionUID = 1L;
    private ArrayList<String> userId;
    private ArrayList<String> username;
    private ArrayList<String> passwords;
    private ArrayList<String> createBillboard;
    private ArrayList<String> editBillboard;
    private ArrayList<String> scheduleBillboard;
    private ArrayList<String> editUsers;


    public UserListReply (boolean successful, ArrayList<String> userId, ArrayList<String> username, ArrayList<String> passwords,
        ArrayList<String> createBillboard, ArrayList<String> editBillboard, ArrayList<String> scheduleBillboard,
        ArrayList<String> editUsers) {
        super(successful);
        this.userId = userId;
        this.username = username;
        this.passwords = passwords;
        this.createBillboard = createBillboard;
        this.editBillboard = editBillboard;
        this.scheduleBillboard = scheduleBillboard;
        this.editUsers = editUsers;

    }
    public ArrayList<String> getUserId(){return userId;}
    public ArrayList<String> getUsername() { return username; }
    public ArrayList<String> getPasswords() { return passwords; }
    public ArrayList<String> getCreateBillboad() { return createBillboard; }
    public ArrayList<String> getEditBillboard() { return editBillboard; }
    public ArrayList<String> getScheduleBillboard() { return scheduleBillboard; }
    public ArrayList<String> getEditUsers() { return editUsers; }

}