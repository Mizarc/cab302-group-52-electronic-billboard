package org.cab302.group52.networkdata.billboard;

import java.io.Serializable;

/**
 * Sent by the client to request the removal of a specified billboard.
 */
public class BillboardRemoveRequest implements Serializable {
    private static final long serialVersionUID = 1L;
    private final String id;

    public BillboardRemoveRequest(String id){
        this.id = id;
    }

    public String getId() { return id; }
}
