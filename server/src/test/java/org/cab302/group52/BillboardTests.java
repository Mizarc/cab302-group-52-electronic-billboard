package org.cab302.group52;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.util.ArrayList;

import org.cab302.group52.networkdata.billboard.*;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class BillboardTests {
    private static final String testName = "billy";
    private static Socket socket;
    private ObjectOutputStream output;
    private ObjectInputStream input;
    private String billboardId;
    private String billboardData;

    /** 
     * Tests a response when a new billboard with data is sent to the server.
     * 
     * @throws IOException
     * @throws ClassNotFoundException
     */
    @Test
    public void addBillboard() throws IOException, ClassNotFoundException {
        // Get test XML file from resources
        File file = new File(this.getClass().getClassLoader().getResource("testboard1.xml").getFile());
        String xml = new String(Files.readAllBytes(file.toPath()));

        // Send Request
        BillboardAddRequest request = new BillboardAddRequest("root", testName, xml);
        output.writeObject(request);
        output.flush();

        // Receive Reply
        input = new ObjectInputStream(socket.getInputStream());
        Object object = input.readObject();
        if (object instanceof BillboardAddReply) {
            BillboardAddReply reply = (BillboardAddReply) object;
            assertTrue(reply.isSuccessful());
            return;
        }
    }
    
    /** 
     * Expect to find billboard by the name of 'billy' after running the add test
     * 
     * @throws IOException
     * @throws ClassNotFoundException
     */
    @Test
    public void findBillboard() throws IOException, ClassNotFoundException {
        // Send Request
        BillboardListRequest request = new BillboardListRequest();
        output.writeObject(request);
        output.flush();

        // Receive Reply
        input = new ObjectInputStream(socket.getInputStream());
        Object object = input.readObject();
        if (object instanceof BillboardListReply) {
            BillboardListReply reply = (BillboardListReply) object;
            ArrayList<String> billboardIds = reply.getBillboardId();
            ArrayList<String> billboardNames = reply.getBillboardName();
            Boolean nameCheck = billboardNames.contains(testName);
            assertTrue(nameCheck);
            if (nameCheck) {
                Integer index = billboardNames.indexOf(testName);
                billboardId = billboardIds.get(index);
            }
            return;
        }
    }

    /** 
     * Expect to find billboard by the name of 'billy' after running the add test
     * 
     * @throws IOException
     * @throws ClassNotFoundException
     */
    @Test
    public void getBillboardData() throws IOException, ClassNotFoundException {
        // Get test XML file from resources
        File file = new File(this.getClass().getClassLoader().getResource("testboard1.xml").getFile());
        String xml = new String(Files.readAllBytes(file.toPath()));

        // Send Request
        BillboardXMLRequest request = new BillboardXMLRequest(billboardId);
        output.writeObject(request);
        output.flush();

        // Receive Reply
        input = new ObjectInputStream(socket.getInputStream());
        Object object = input.readObject();
        if (object instanceof BillboardXMLReply) {
            BillboardXMLReply reply = (BillboardXMLReply) object;
            assertEquals(xml, reply.getXML());
            return;
        }
    }

    /** 
     * Expect to find billboard by the name of 'billy' after running the add test
     * 
     * @throws IOException
     * @throws ClassNotFoundException
     */
    @Test
    public void renameBillboard() throws IOException, ClassNotFoundException {
        // Send Request
        BillboardModifyRequest request = new BillboardModifyRequest(billboardId, "timmy", billboardData);
        output.writeObject(request);
        output.flush();

        // Receive Reply
        input = new ObjectInputStream(socket.getInputStream());
        Object object = input.readObject();
        if (object instanceof BillboardModifyReply) {
            BillboardModifyReply reply = (BillboardModifyReply) object;
            assertTrue(reply.isSuccessful());
            return;
        }
    }
 
    /** 
     * Creates the output stream before every test
     * 
     * @throws IOException
     */
    @BeforeEach
    private void createOutput() throws IOException {
        output = new ObjectOutputStream(socket.getOutputStream());
    }

    /** 
     * Ensure that both input and output streams are reset after every run
     */
    @AfterEach
    private void destroyStreams() {
        output = null;
        input = null;
    }

    /** 
     * Run the server in a loop and establish client socket.
     * 
     * @throws UnknownHostException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    @BeforeAll
    private static void connectSocket() throws UnknownHostException, IOException, ClassNotFoundException {
        Server.setupDatabase();
        Runnable background = new Runnable() {
            public void run() {
                try {
                    Server.serverLoop();
                } catch (ClassNotFoundException | IOException e) {
                    e.printStackTrace();
                }
            }};
        Thread sampleThread = new Thread(background);
        sampleThread.start();
        socket = new Socket("localhost", 3129);
    }
    
    /** 
     * Close the available socket.
     * 
     * @throws IOException
     */
    @AfterAll
    private static void closeSocket() throws IOException {
        socket.close();
    }
}